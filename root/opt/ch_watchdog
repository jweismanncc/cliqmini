#!/bin/bash
#
# Last Modified By:   Greg Haines
# Last Modified Date: 2015-07-27
#
# This watchdog process will check to ensure the ClareHome process
# is running and responsive. If the ClareHome process is not running
# (and it should be) or is unresponsive, it will clean up any squeezelite
# and mplayer processes and start ClareHome again.
#
##########################################################################

killMusicAndStartClareHome () {
	killall squeezelite
	killall mplayer
	rm /var/run/clarehome.pid
	service clarehome restart
}

# Ensure "/var/log/watchdog" directory exists
mkdir -p /var/log/watchdog

# Add current time to the watchdog file
CUR_TICKS=$(awk '{printf "%.0f", $1 * 100; exit}' /proc/uptime)
echo "$CUR_TICKS" > /var/log/watchdog/claredog.dat

PROCESS=$(pgrep -f equator.ini)

if [ -z $PROCESS ]; then
	# ClareHome process doesn't exist; check if it should be running
	if [ -f /var/run/clarehome.pid ]; then
		killMusicAndStartClareHome
	fi
else
	PROC_TICKS=$(awk "{print \$22; exit}" /proc/$PROCESS/stat)
	if (( $CUR_TICKS - $PROC_TICKS > 39000 )); then
		# ClareHome process exists and has been running for more than ~6.5 minutes; check if telnet port is responding
		CONNECT_RESULT=$(timeout 2 telnet localhost 2323)
		if ! [[ "$CONNECT_RESULT" =~ 'Connected to localhost' ]]; then
			# ClareHome is running but unresponsive, kill it and start it again
			kill -9 $PROCESS
			sleep 2
			killMusicAndStartClareHome
		fi
	fi
fi
