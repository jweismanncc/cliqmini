#!/bin/bash

EQVER=5.1.0
EQFILE=assembly-fullload-cliqmini-$EQVER.zip

EQUATOR=http://nexus.clarecontrols.com:8001/nexus/service/local/repositories/releases/content/com/clarecontrols/equator/assembly-fullload-cliqmini/$EQVER/$EQFILE

Off='\033[0m'       # Text Reset
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow

# Save out our start home directory so we can get back to it easily.
HOMEDIR=`pwd`

# Setup log files, set it to append
LOG_FILE=createmini.log
exec > >(tee -a ${LOG_FILE} )
exec 2> >(tee -a ${LOG_FILE} >&2)

# We need to perform some basic stuff.  This is all ran as a THE root user
#	and we need to validate that.  This is because we will be
#	making sure the Pi user is moved over to the clarecontrols user.

# Method to colorize the text output.
function colorEcho() 
{
	local exp=$1
	local color=$2

	echo -e $color$exp ${Off}
}

# Method to check for the user executing the script to be root user or ID 0
function check4root 
{
	if [[ $EUID -ne 0 ]]; then
		colorEcho "ERROR - This script must be run as root" $Red
		exit 1
	fi

	colorEcho "We are root user" $Green
}

# Check for a valid internet connection
function check4internet 
{
	echo "check4internet - Called"
	# check for wget just to be sure we have it installed
	WGET=`which wget`
	if [ -z "$WGET" ]; then
		colorEcho "ERROR - wget binary not available, please check your setup ensuring it is installed" $Red
		exit 1
	fi

	wget -q --spider http://google.com
	if [ $? -eq 0 ]; then
		colorEcho "Connected to internet" $Green
	else
		colorEcho "ERROR - Cannot connect to internet, please check for valid internet connection" $Red
		exit 1
	fi
}

# check for the git binary, if it is not installed, we install it automatically.
function check4git 
{
	echo "check4git - Called"
	GIT=`which git`
	if [ -z "$GIT"]; then
		colorEcho "WARNING - Git not installed, installing now...." $Yellow
		apt-get -y install git
	fi
	echo "git binary found..."
}

# All prequisites software installed.
function checkprequisites 
{
	echo "checkprequisites - Called"
	check4root
	check4internet
	#check4git
}

# we need to modify the Pi user here to clarecontrols.
function setupclareuser 
{
	echo "Now we setup the clarecontrols user"
	# change the username
	usermod -l clarecontrols pi
	# change the group
	groupmod -n clarecontrols pi
	# change the home directory
	usermod -d /home/clarecontrols -m clarecontrols
	# change the password
	echo "clarecontrols:penn@7519@CC" | chpasswd 
	colorEcho "clarecontrols user setup completed" $Green
}

#update and install base packages
function updatePi 
{
	apt-get -y update
	apt-get -y upgrade
	apt-get -y install bootp xinetd oracle-java8-jdk avahi-utils ifplugd isc-dhcp-server
	update-rc.d isc-dhcp-server disable
}

# install packages
function installpackages 
{
	echo "Installing support packages..."
	dpkg -i ./supportpkgs/weavedconnectd-clare-1.3.04.deb

	rm /usr/bin/weavedinstaller_clare
	cp ./supportpkgs/weavedinstaller_clare /usr/bin
}

# build the wireingPi library
function buildWiringPi 
{
	echo "Building the wiringPi library..."
	cd ./wiringPi
	./build
	cd $HOMEDIR
}

# create the misc directories needed
function createdirectories 
{
	echo "Creating directories..."
	mkdir /home/clarecontrols/Music
	mkdir /var/log/clarehome
	mkdir /usr/local/clarecontrols
	mkdir /usr/local/clarecontrols/clarehome-3.0
	mkdir /root/.config
	mkdir /opt/.mfg
}

# change permissions of directories
function changepermissions 
{
	echo "Setting permissions..."
	chown -Rf clarecontrols:clarecontrols /home/clarecontrols
	chown -Rf clarecontrols:clarecontrols /usr/local/clarecontrol
	chown -Rf clarecontrols:clarecontrols /var/log/clarehome
}

# Move the manufature node.js project in place
function setupmfgweb 
{
	echo "Setting up mfgtest suite..."
	cp -rf ./mfgtest /root/.config/.
}

#install node.js package
function installnodeJS 
{
	echo "Installing node.js server..."
	tar xvf ./supportpkgs/nodejs-5.5.0.tgz -C /usr/local
}

# get the current released version of equator
function installclarehome 
{
	echo "Please enter your nexus username"
	read USER
	echo "Please enter your nexus password"
	read PASSWORD

	wget --user=$USER --password=$PASSWORD "$EQUATOR"

	# now untar the zip to the correct directory
	unzip $EQFILE -d /usr/local/clarecontrols/clarehome-3.0

	# Setup the equator.prs, equator.ops, equator.prod files
	TEMP=/tmp
	DIR=/usr/local/clarecontrols/clarehome-3.0/osgi/bin/vms
	sed 's/mprmbeta/provisioning/g' $DIR/equator.prs > $TEMP/equator.tmp
	sed 's/fusiontest/fusion/g' $TEMP/equator.tmp > $DIR/equator.prod
	sed 's/mprmbeta/mprmops/g' $DIR/equator.prs > $TEMP/equator.tmp2
	sed 's/fusiontest/fusion/g' $TEMP/equator.tmp2 > $DIR/equator.ops
	cp $DIR/equator.ops $DIR/equator.prs

sync
}

# put all the extra files into location such as startup scripts, and other configurations files
function setupRoot
{
	cp -rf ./root/* /
	mv -f /root/* /root/.config
}

# The main execution thread function.
function main 
{
	checkprequisites
	setupclareuser
	updatePi
	installpackages
	installnodeJS
	buildWiringPi
	createdirectories
	setupmfgweb
	installclarehome
	setupRoot
	changepermissions
}

main
