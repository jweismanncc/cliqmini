// DOM Ready =================================================================
$(document).ready(function() 
{
	// Populate the table date
	populateTable();

	// Populate the mac address
	//	getUpdateMac();

	// Read the cmdline.txt file in and see if we already have a MAC address.
	readcmdline();

	// Just for testing, save the mini data
	//savedata();
});

// Functions ================================================================

// Fill table with data
function populateTable() 
{
	// Empty content string
	var tableContent = '';

	// Get data
	$.getJSON( '/users/minitest', function(data) {

		// Try a new parse method
		for( var exKey in data) {
			tableContent += '<tr>';
			tableContent += '<td>' + exKey + '</td>';
			tableContent += '<td id=' + exKey + '>' + data[exKey] + '</td>';
			tableContent += '</tr>';
		}

		// Inject the content into existing HTML table
		$('#paramList table tbody').html(tableContent);
	});
};

// Get Mac Address
function getUpdateMac()
{
	// Retrieve the mac address
	$.getJSON('/users/macaddr', function(data){

		console.log(data);
		$('#macaddress').text(data.macaddr.replace(/(.{2})/g,"$1:").slice(0, -1));
	});
};

function readcmdline() {

	var returndata;

	// Retrieve the old mac address
	$.getJSON('/users/curmac', function(data){
		console.log(data);
		$('#macaddress').text(data.macaddr);

		console.log("Mac Address Stored..." + $('#macaddress').text());

		// Check to see if it one of clarecontrols mac address's
		var mac = data.macaddr;
		var newmac = mac.replace(/:/g, '');
		var numMAC = parseInt(newmac, 16);

		if ( 97201545674752 <= numMAC && numMAC >= 97201546002431 ) {
			console.log("Mac address is not clarecontrols reassigning mac....");
			getUpdateMac();
		} else {
			console.log("Mac address is clarecontrols reusing mac....");
		}

		// Revert the :'s back in
		var newstring = numMAC.toString(16);
		var tempstr = newstring.replace(/(.{2})/g, "$1:").slice(0, -1);
		
	});

};

function savedata() {

	// Disable the app save button
	$('#savebtn').attr("disabled", "disabled");

	var data = {};

	data['MacAddress'] 			= $("#macaddress").text();
	data['Hostname'] 			= $("#Hostname").text();
	data['Type']				= $("#Type").text();
	data['Platform']			= $("#Platform").text();
	data['Architecture']		= $("#Architecture").text();
	data['Release']				= $("#Release").text();
	data['OS-Uptime']			= $("#OS-Uptime").text();
	data['OS-load-average']		= $("#OS-load-average").text();
	data['Total-Ram']			= $("#Total-Ram").text();
	data['Free-Ram']			= $("#Free-Ram").text();
	data['Temp-Directory']		= $("#Temp-Directory").text();
	data['Z-Wave']				= $("#Z-Wave").text();
	data['WiFi']				= $("#WiFi").text();

	$('#console').text('Saving data...');

	$.post('/users/savemini', data, function(info) {
		$('#console').text('Done');
	});

	
};
