var os = require('os');
var exec = require('child_process').exec
var express = require('express');
var mysql = require("mysql");
var fs = require('fs');
var router = express.Router();
var child;


router.get('/minitest', function(req, res) {

	var jsonObj = {};

	//  Hostname
	jsonObj['Hostname'] = os.hostname();

	// Type
	jsonObj['Type'] = os.type();

	// Platform
	jsonObj['Platform'] = os.platform();

	// Architecture
	jsonObj['Architecture'] = os.arch();

	// Release
	jsonObj['Release'] = os.release();

	// Uptime
	jsonObj['OS-Uptime'] = os.uptime() + " sec";

	// Load Average
	jsonObj['OS-load-average'] = os.loadavg();

	// Total Memory
	jsonObj['Total-Ram'] = (os.totalmem()/1024/1024) + " mb";

	// Free Memory
	jsonObj['Free-Ram'] = (os.freemem()/1024/1024) + " mb";

	// Temp Directory
	jsonObj['Temp-Directory'] = os.tmpdir();

	// Check to see if the Sigma Z-Wave device is present
	exec("lsusb | grep 0658:0200", function (error, stdout, stderr) 
	{

    	var result = stdout.toString();
    	if ( result.length === 0 )
    	{
			jsonObj['Z-Wave'] = 'Fail';
    	}
    	else
    	{
			jsonObj['Z-Wave'] = 'Pass';
    	}

		// Nesting the last test call, kind of dumb, but it works.
    	exec ("lsusb | grep 148f:7601", function (error, stdout, stderr)
    	{
        	var result = stdout.toString();
        	if ( result.length === 0 )
        	{
				jsonObj['WiFi'] = 'Fail';
        	}
        	else
        	{
				jsonObj['WiFi'] = 'Pass';
        	}
			res.json(jsonObj);
    	});
	});
});

router.get('/macaddr', function(req, res) {

	var con = mysql.createConnection({
		host: "54.201.88.104",
		//host: "10.100.100.100",
		user: "clarecontrols",
		password: "claresql",
		database: "cliqmini"
	});
	
	con.query('SELECT * FROM current_info where curr_id = 1', function ( err, rows ) {
		if(err) throw err;

		var string = JSON.stringify(rows);
		var json = JSON.parse(string);

		var macaddr = Number(json[0].cur_mac);

		var macaddr = macaddr + 1;

		con.end();

		var jsonObj = {};
		jsonObj['macaddr'] =  macaddr.toString(16).toUpperCase();
		res.json(jsonObj);

	});
});

router.get('/savemacaddr', function(req, res) {

	var con = mysql.createConnection({
		host: "54.201.88.104",
		//host: "10.100.100.100",
		user: "clarecontrols",
		password: "claresql",
		database: "cliqmini"
	});
	
	console.log("DB Connection created");

	con.query('SELECT * FROM current_info where curr_id = 1', function ( err, rows ) {
		if(err) throw err;

		console.log("We got a mac from database");

		var string = JSON.stringify(rows);
		var json = JSON.parse(string);

		var macaddr = Number(json[0].cur_mac);

		var macaddr = macaddr + 1;

		// Now we update the database.
		con.query('UPDATE current_info SET cur_mac = ? WHERE curr_id = 1',[macaddr], function (err, result){
			if (err) throw err;
			console.log("updated the mac address with new one");
		});

		con.end();

		res.sendStatus(200);
	});
});

router.get('/curmac', function (req, res) {

	var fileName = "/boot/cmdline.txt";

	fs.exists(fileName, function(exists) {
		if (exists) {
			fs.stat(fileName, function(error, stats) {
				fs.open(fileName, "r", function(error, fd) {
					var buffer = new Buffer(stats.size);

					fs.read(fd, buffer, 0, buffer.length, null, function(error, bytesRead, buffer) {
						var data = buffer.toString("utf8", 0, buffer.length);
						fs.close(fd);

						// Search the string for a mac address
						var substr = data.search("smsc95");
						if (substr != -1) {
							var tmpmacStr = data.toString().substr(substr);
							//console.log(tmpmacStr.split('=')[1].replace(/:/g, ''));
							console.log(tmpmacStr.split('=')[1]);
							var macaddr = tmpmacStr.trim().split('=')[1];
							var jsonObj = {};
							jsonObj['macaddr'] =  macaddr.toString(16).toUpperCase();
							res.json(jsonObj);
							//res.sendStatus('{"macaddr":"' + macaddr.toString(16).toUpperCase() + '"}');
						}
						else {
							// We don't have a clare assigned mac address we need to pull the Pi version.
							var jsonObj = {};

							// get the mac address
							exec ("cat /sys/class/net/eth0/address", function (error, stdout, stderr) {
								if ( stdout != null ) {
									console.log ( stdout );
									jsonObj['macaddr'] = stdout.toString().trim();
								}
								res.json(jsonObj); 
							});
						}
					});
				});
			});
		}
	});
});

router.get('/detach', function(req, res) 
{
	exec ("/root/.config/detach", function (error, stdout, stderr)
	{
		if ( error != null )
		{
			res.sendStatus(500).send(error);
		} else {
			res.sendStatus(200);
		}
	});
});

router.post('/savemini', function(req, res) {

	// Just dump info for dubugging.
	console.log (req.body);
	console.log (req.body.MacAddress);


	// Save the new mac address out to the device so it will be a clare mac address
	// Safety check to ensure it is not already a clare mac address.
	var fileName = "/boot/cmdline.txt";

	fs.exists(fileName, function(exists) {
		if (exists) {
			fs.stat(fileName, function(error, stats) {
				fs.open(fileName, "r+", function(error, fd) {
					var buffer = new Buffer(stats.size);

					fs.read(fd, buffer, 0, buffer.length, null, function(error, bytesRead, buffer) {
						var data = buffer.toString("utf8", 0, buffer.length);
						//fs.close(fd);

						// Search the string for a mac address, if we dont' have it we put the mac in there.
						var substr = data.search("smsc95");
						if (substr == -1) {
							// Write the new data out
							fs.writeFileSync(fd, data.trim() + " smsc95xx.macaddr=" + req.body.MacAddress + os.EOL);
							fs.close(fd);
						};
					});
				});
			});
		};
	});

	// Save the data to the database
	var con = mysql.createConnection({
		host: "54.201.88.104",
		//host: "10.100.100.100",
		user: "clarecontrols",
		password: "claresql",
		database: "cliqmini"
	});

	console.log("Database connection created");

	// Create data to post to database
	var mfgdate = new Date();

	// Create query
	var query = "INSERT INTO mini_results (mac_address, date, data) VALUES ('" + req.body.MacAddress + "', '" + mfgdate + "', '" + req.body + "');";
	con.query(query, function (err, result) {

				
		con.end();

		console.log(result);

		if ( err != null ) {
			console.log(err);
			res.sendStatus(500).send(err);
		}
		
	});


	// Run the scripts to clean and setup the device
	exec("/root/.config/pre4mfg", function (error, stdout, stderr)
	{
		if ( stdout !== null )
		{
			// script completed ok, now we need 2 run the second script.
			exec("/root/.config/mfgsetup " + req.body.MacAddress, function (error, stdout, stderr)
			{
				if ( stdout !== null )
				{
					exec("weavedinstaller_clare " + req.body.MacAddress, function (error, stdout, stderr)
					{
						if ( stdout !== null )
						{
							exec("/root/.config/cleanup", function (error, stdout, stderr)
							{
								if ( stdout !== null )
								{
									exec ("sync", function (error, stdout, stderr)
									{
										if ( error != null )
										{
											res.sendStatus(500).send(error);
										} else {
											res.sendStatus(200);
										}
									});
								}
							});
						}
					});
				}
			});
		}
	});

});


module.exports = router;
