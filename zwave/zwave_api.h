/*---------------------------------------------------------------------------
**
** MODULE NAME:     zwave_api.h
**
** COPYRIGHT:       Copyright (c) 2014, Clare Controls, Inc.
**                  All Rights Reserved.
**
**                  NOTICE: THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF
**                  CLARE CONTROLS, INC., AND SHALL NOT BE REPRODUCED, COPIED
**                  IN WHOLE OR IN PART ADAPTED, MODIFIED, OR
**                  DISSEMINATED WITHOUT THE EXPRESS WRITTEN CONSENT
**                  OF CLARE CONTROLS, INC. ANY REDISTRIBUTION OR
**                  PUBLICATION TO UNAUTHORIZED PERSONS IS STRICTLY
**                  PROHIBITED BY U.S. AND INTERNATIONAL COPYRIGHT LAW.
**
** DESCRIPTION:     Zwave API (interface).
**
** AUTHOR:          M. Grosberg / G. Tozzi
**
** CREATION DATE:   Jul  9, 2014
**
**---------------------------------------------------------------------------
**
** HISTORY OF CHANGE  
**
** -----+--------+------------------------------------------+------+---------
** VERS | DATE   | DESCRIPTION OF CHANGE                    | INIT | PR#
** =====+========+==========================================+======+=========
**  001 |07/09/14| File creation.                           | MYG  |
** -----+--------+------------------------------------------+------+---------
** -----+--------+------------------------------------------+------+---------
**-------------------------------------------------------------------------*/

#ifndef ZWAVE_API
#define ZWAVE_API

#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>

#define ZWAVE_RANDOM_HOMEID_START_RANGE 0xC0000000UL
#define ZWAVE_RANDOM_HOMEID_END_RANGE   0xFFFFFFFEUL

typedef enum 
{
  RESET_H, 
  RESET_L,
  SPI_CLK_PULSE
} ZWAVE_RESET_COMMAND;

typedef uint32_t ZWAVE_HOME_ID;

bool zwave_reset_logic(ZWAVE_RESET_COMMAND command);
bool zwave_open(void);
void zwave_close(void);
bool zwave_write_flash(const uint8_t *p_data, size_t data_len);

bool zwave_calibrate_tx(const uint8_t *p_calibration_fw, size_t fw_len,
                        uint8_t *p_ccal);

bool zwave_test_peer(unsigned int *p_count);
bool zwave_get_homeid(ZWAVE_HOME_ID *p_home_id);
bool zwave_factory_default(void);
bool zwave_get_api(char *api);
bool zwave_set_rfpower(uint8_t *power);
bool zwave_get_rfpower(uint8_t *power);

bool zwave_read_ext_nvm(void);
bool zwave_write_ext_nvm(const uint8_t *p_data, size_t data_len);

#endif
