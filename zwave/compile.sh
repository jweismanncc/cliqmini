#! /bin/sh

if test "x$1" = "x"; then
  echo Usage: compile.sh [DEST]
  exit 1
fi

arm-linux-gnueabihf-gcc -o "$1/zwave" -Wall -Os     \
   -DRASPI zwave_api.c zwave_tool.c -lm
exit $?

