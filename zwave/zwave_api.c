/*---------------------------------------------------------------------------
**
** MODULE NAME:     zwave_api.c
**
** COPYRIGHT:       Copyright (c) 2014, Clare Controls, Inc.
**                  All Rights Reserved.
**
**                  NOTICE: THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF
**                  CLARE CONTROLS, INC., AND SHALL NOT BE REPRODUCED, COPIED
**                  IN WHOLE OR IN PART ADAPTED, MODIFIED, OR
**                  DISSEMINATED WITHOUT THE EXPRESS WRITTEN CONSENT
**                  OF CLARE CONTROLS, INC. ANY REDISTRIBUTION OR
**                  PUBLICATION TO UNAUTHORIZED PERSONS IS STRICTLY
**                  PROHIBITED BY U.S. AND INTERNATIONAL COPYRIGHT LAW.
**
** DESCRIPTION:     Zwave API (implementation).
**
** AUTHOR:          M. Grosberg / G. Tozzi
**
** CREATION DATE:   Jul  9, 2014
**
**---------------------------------------------------------------------------
**
** HISTORY OF CHANGE  
**
** -----+--------+------------------------------------------+------+---------
** VERS | DATE   | DESCRIPTION OF CHANGE                    | INIT | PR#
** =====+========+==========================================+======+=========
**  001 |07/09/14| File creation.                           | MYG  |
** -----+--------+------------------------------------------+------+---------
** -----+--------+------------------------------------------+------+---------
**-------------------------------------------------------------------------*/

#include "zwave_api.h"
#include <assert.h>
#include <ctype.h>
#include <termios.h>
#include <time.h>
#include <math.h>
#include <poll.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#define BIT(x)        (1 << (x))
#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

/****************************************************************************/
/*                                CRC-16                                    */
/****************************************************************************/

#define P_CCITT    0x1021

static unsigned short crc_tabccitt[256];

static unsigned short update_crc_ccitt(unsigned short crc, char c)
{
    unsigned short tmp, short_c;

    short_c  = 0x00ff & (unsigned short) c;

    tmp = (crc >> 8) ^ short_c;
    crc = (crc << 8) ^ crc_tabccitt[tmp];

    return crc;
} 

static void init_crcccitt_tab(void) 
{
    int i, j;
    unsigned short crc, c;

    for (i=0; i<256; i++) {

        crc = 0;
        c   = ((unsigned short) i) << 8;

        for (j=0; j<8; j++) {

            if ( (crc ^ c) & 0x8000 ) crc = ( crc << 1 ) ^ P_CCITT;
            else                      crc =   crc << 1;

            c = c << 1;
        }

        crc_tabccitt[i] = crc;
    }

}  

/****************************************************************************/
/*                           MODULE INTERFACE                               */
/****************************************************************************/

#define ZWAVE_RESET         "/proc/zwave_reset"
#define ZWAVE_SPI           "/dev/spidev1.0"
#define SPI_CLK_FREQ        100000
#define SPI_BITS_PER_WORD   8
#define ZWAVE_BAUDRATE      B115200

#ifdef RASPI
#define ZWAVE_UART          "/dev/ttyACM0"
#else
#define ZWAVE_UART          "/dev/ttymxc1"
#endif


static int spi_fd, reset_fd, uart_fd;

static bool spi_transfer(uint8_t *tx, uint8_t *rx, uint8_t len)
{
  struct spi_ioc_transfer tr = 
  {
    .tx_buf = (unsigned long)tx,
    .rx_buf = (unsigned long)rx,
    .len = len,
    .delay_usecs = 0,
    .speed_hz = SPI_CLK_FREQ,
    .bits_per_word = SPI_BITS_PER_WORD,
  };

  if (ioctl(spi_fd, SPI_IOC_MESSAGE(1), &tr) < 1)
  {
    fprintf(stderr, "SPI Transfer error: %s\n", strerror(errno));
    return false;
  }

  return true;
}

static bool check_response(const uint8_t *data, size_t len)
{
  uint8_t xor = 0xff;
  size_t  i;

  assert(len >= 4);

  if (data[0] != 6)
  {
    fprintf(stderr, "Did not get an ACK byte\n");
    return false;
  }

  for(i = 2; i < (len - 1); i++)
    xor ^= data[i];

  if (xor == data[len - 1])
    return true;
  else
  {
    fprintf(stderr, "Z-wave RX checksum mismatch\n");
    return false;
  }
}

static bool zwave_uart_write(const uint8_t *data, size_t len)
{
  uint8_t xor = 0xff;
  size_t  i;

  for(i = 1; i < len; i++)
    xor ^= data[i];

  if (write(uart_fd, data, len) != (ssize_t )len)
  {
    fprintf(stderr, "UART write error: %s\n", strerror(errno));
    return false;
  }

  if (write(uart_fd, &xor, 1) != (ssize_t )1)
  {
    fprintf(stderr, "UART write error (checksum): %s\n", strerror(errno));
    return false;
  }

  return true;
}

static bool zwave_uart_ack(void)
{
  const char ack = 0x06;
  size_t     len = sizeof(ack);

  if (write(uart_fd, &ack, len) != (ssize_t )len)
  {
    fprintf(stderr, "UART write error: %s\n", strerror(errno));
    return false;
  }

  return true;
}

static bool zwave_uart_flush_rx(void)
{
  int     old_flags;
  char    junk[64];
  ssize_t rr;
  bool    rc = true;

  /* First set non-blocking mode. */
  old_flags = fcntl(uart_fd, F_GETFL, 0);
  fcntl(uart_fd, F_SETFL, old_flags | O_NONBLOCK);

  /* Read off anything queued up in the buffer. */
  do
  {
    rr = read(uart_fd, junk, sizeof(junk));
    if (rr < 0)
    {
      if (errno != EAGAIN)
        rc = false;
      break;
    }
  } while (rr > 0);

  if (!rc)
    fprintf(stderr, "Error flushing Z-wave RX buffer.\n");

  /* Restore the old file flags. */
  fcntl(uart_fd, F_SETFL, old_flags);
  return rc;
}

bool zwave_uart_read_timeout(uint8_t *data, size_t len, unsigned int *p_to)
{
  int            old_flags;
  struct pollfd  pfd;
  time_t         start;
  bool           rc = true;
  unsigned int   to = *p_to;

  /* First set non-blocking mode. */
  old_flags = fcntl(uart_fd, F_GETFL, 0);
  fcntl(uart_fd, F_SETFL, old_flags | O_NONBLOCK);

  /* Setup the poll descriptor. */
  pfd.fd      = uart_fd;
  pfd.events  = POLLIN | POLLERR | POLLHUP;
  pfd.revents = 0;

  /* Note the starting time. */
  time(&start);

  /* Read bytes. */
  while (len > 0)
  {
    unsigned int  wait_time;
    time_t        now;
    ssize_t       res;

    /* Compute the timeout. */
    time(&now);
    wait_time = ceil(difftime(now, start) * 1000);
    if (wait_time >= to)
    {
      rc = false;
      break;
    }
    
    /* Wait for the timeout. */
    pfd.revents = 0;
    if ((poll(&pfd, 1, to - wait_time) != 1)
    ||  ((pfd.revents & POLLIN) == 0))
    {
      rc = false;
      break;
    }

    /* Collect data. */
    res = read(uart_fd, data, len);
    if (res < 0)
    {
      rc = false;
      break;
    }

    /* Adjust the pointers for as much as we've read. */    
    data += (size_t )res;
    len  -= (size_t )res;
  }

  if (rc)
  {
    time_t  now;

    /* Update the timeout parameter to reflect the time spent reading. */
    time(&now);
    unsigned int elapsed = (unsigned int )difftime(now, start) * 1000;
    if (elapsed > *p_to)
      *p_to = 0;
    else 
      *p_to -= elapsed;
  }
  else
    fprintf(stderr, "Timeout waiting for Z-wave to respond.\n");

  /* Restore the old file flags. */
  fcntl(uart_fd, F_SETFL, old_flags);
  return rc;
}

bool zwave_reset_logic(ZWAVE_RESET_COMMAND command)
{
  ssize_t     rc;
  const void *p_command;

  static const char reset_high = '1';
  static const char reset_low  = '0';
  static const char spi_clk    = 'R';

  switch (command)
  {
    case RESET_H:       p_command = &reset_high; break;
    case RESET_L:       p_command = &reset_low;  break;
    case SPI_CLK_PULSE: p_command = &spi_clk;    break;

    default:
      assert(0);
  }

  rc = write(reset_fd, p_command, 1);
  if (rc != 1)
  {
    fprintf(stderr, "Error changing reset state: %s\n", strerror(errno));
    return (false);
  }

  return (true);
}

bool zwave_open(void)
{
  struct termios tty;

//TODO
#ifndef RASPI
  spi_fd = open(ZWAVE_SPI, O_RDWR);
  if (spi_fd < 0)
  {
    fprintf(stderr, "Error opening SPI: %s\n", strerror(errno));
    goto spi_fail;
  }

  reset_fd = open(ZWAVE_RESET, O_WRONLY);
  if (reset_fd < 0)
  {
    fprintf(stderr, "Error opening reset: %s\n", strerror(errno));
    goto reset_fail;
  }
#endif

  memset(&tty, 0, sizeof tty);
  uart_fd = open(ZWAVE_UART, O_RDWR | O_NOCTTY);
  if (uart_fd < 0)
  {
    fprintf(stderr, "Error opening UART: %s\n", strerror(errno));
    goto uart_fail;
  }

  if (tcgetattr(uart_fd, &tty) != 0)
  {
    fprintf(stderr, "Error getting attributes: %s\n", strerror(errno));
    goto uart_fail;
  }

  cfsetospeed(&tty, ZWAVE_BAUDRATE);
  cfsetispeed(&tty, ZWAVE_BAUDRATE);
  tty.c_cc[VTIME] = 0;
  tty.c_cc[VMIN]  = 1;

  tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR |
                   IGNCR | ICRNL | IXON | IXANY);
  tty.c_oflag &= ~(OPOST);
  tty.c_lflag &= ~(ECHO | ECHONL | ECHOE | ICANON | ISIG | IEXTEN);
  tty.c_cflag &= ~(CSTOPB | CSIZE | CRTSCTS | PARENB | PARODD);
  tty.c_cflag |=  CS8;

  tcflush(uart_fd, TCIFLUSH);
  if (tcsetattr(uart_fd, TCSANOW, &tty) != 0)
  {
    fprintf(stderr, "Error setting attributes: %s\n", strerror(errno));
    goto uart_fail;
  }

  init_crcccitt_tab();
  return true;

uart_fail:
  close(reset_fd);
#ifndef RASPI
reset_fail:
  close(spi_fd);
spi_fail:
#endif
  return false;
}

void zwave_close(void)
{
#ifndef RASPI
  close(spi_fd);
  close(reset_fd);
#endif
  close(uart_fd);
}

/****************************************************************************/
/*                             PROGRAMMER                                   */
/****************************************************************************/

#define ZWAVE_SYNC_RETRY       33
#define ZWAVE_MSG_LEN          4
#define ZWAVE_MAX_BYTE_WRITE   3

/*
 * All programming interface commands are 4 bytes long.
 * The following defines are used for commands sent to Z-Wave
 * module only. If the UART programming interface were being used,
 * the zero byte would be checked for echo back. But we are using
 * SPI which on the zero byte, echoes back the last byte from a
 * previous transfer.
 */
#define ENABLE_INTERFACE_0         0xAC
#define ENABLE_INTERFACE_1         0x53
#define ENABLE_INTERFACE_2         0xAA
#define ENABLE_INTERFACE_3         0x55

#define ERASE_SECT_0               0x0B
#define ERASE_SECT_1               0x00
#define ERASE_SECT_2               0x00
#define ERASE_SECT_3               0x00

#define ERASE_CHIP_0               0x0A
#define ERASE_CHIP_1               0x00
#define ERASE_CHIP_2               0x00
#define ERASE_CHIP_3               0x00

#define WRITE_FLASH_SECT_0         0x20
#define WRITE_FLASH_SECT_1         0x00
#define WRITE_FLASH_SECT_2         0x00
#define WRITE_FLASH_SECT_3         0x00

#define CHECK_STATE_0              0x7F
#define CHECK_STATE_1              0xFE
#define CHECK_STATE_2              0x00
#define CHECK_STATE_3              0x00

#define CHECK_CRC_0                0xC3
#define CHECK_CRC_1                0x00
#define CHECK_CRC_2                0x00
#define CHECK_CRC_3                0x00

#define READ_SRAM_0                0x06
#define READ_SRAM_1                0x00
#define READ_SRAM_2                0x00
#define READ_SRAM_3                0x00

#define WRITE_SRAM_0               0x04
#define WRITE_SRAM_1               0x00
#define WRITE_SRAM_2               0x00
#define WRITE_SRAM_3               0x00

#define WRITE_NVR_0                0xFE
#define WRITE_NVR_1                0x00
#define WRITE_NVR_2                0x00
#define WRITE_NVR_3                0x00

#define READ_NVR_0                 0xF2
#define READ_NVR_1                 0x00
#define READ_NVR_2                 0x00
#define READ_NVR_3                 0x00

#define CONTINUE_WRITE_0           0x80
#define CONTINUE_WRITE_1           0x00
#define CONTINUE_WRITE_2           0x00
#define CONTINUE_WRITE_3           0x00

#define ZWAVE_3502_SRAM_SZ         2048
#define ZWAVE_3502_SECTOR_SZ       2048
#define ZWAVE_3502_SECTOR_CNT      64

#define ZWAVE_3502_NVR_SZ          256
#define ZWAVE_3502_NVR_START_ADDR  0x00
#define ZWAVE_3502_NVR_END_ADDR    0xFF

/* Default values for the NVR. */
#define NVR_RBAP_VAL               0x00
#define NVR_REV_VAL                0x01
#define NVR_PINS_VAL               0x01
#define NVR_SAWC_VAL0              0x0D
#define NVR_SAWC_VAL1              0xE8
#define NVR_SAWC_VAL2              0x8C
#define NVR_SAWB_VAL               0x1B
#define NVR_NVMT_VAL               0x01
#define NVR_NVMCS_VAL              0x04              
#define NVR_NVMS_VAL0              0x00
#define NVR_NVMS_VAL1              0x20
#define NVR_NVMP_VAL0              0x00
#define NVR_NVMP_VAL1              0x40

/* NVR Addresses. */
#define NVR_RBAP                   0x08
#define NVR_REV                    0x10
#define NVR_CCAL                   0x11
#define NVR_PINS                   0x12
#define NVR_NVMCS                  0x13
#define NVR_SAWC                   0x14
#define NVR_SAWC_LEN               3
#define NVR_SAWB                   0x17
#define NVR_NVMT                   0x18
#define NVR_NVMS                   0x19
#define NVR_NVMS_LEN               2
#define NVR_NVMP                   0x1B
#define NVR_NVMP_LEN               2
#define NVR_UUID                   0x1D
#define NVR_UUID_LEN               16
#define NVR_VID                    0x2D
#define NVR_VID_LEN                2
#define NVR_PID                    0x2F
#define NVR_PID_LEN                2
#define NVR_TXCAL1                 0x31
#define NVR_TXCAL2                 0x32
#define NVR_CRC16                  0x7E
#define NVR_CRC16_LEN              2

#define ZWAVE_3502_TXCAL1_ADDR     0x0FFD
#define ZWAVE_3502_TXCAL2_ADDR     0x0FFE
#define ZWAVE_3502_CRC_START_ADDR  0x10
#define ZWAVE_3502_CRC_END_ADDR    0x7D
#define ZWAVE_3502_CRC_INIT_VAL    0x1D0F

typedef struct
{
  bool crc_busy;
  bool crc_done;
  bool crc_failed;
  bool flash_fsm_busy;
  bool cont_op_refused;
} zwave_state_t;

static bool zwave_sync_programmer(void)
{
  uint8_t  spi_tx[ZWAVE_MSG_LEN];
  uint8_t  spi_rx[ZWAVE_MSG_LEN];
  int      i;

  /* Assert the reset line low to place in programming mode. */
  zwave_reset_logic(RESET_L);
  usleep(5500);

  /* SPI Syncing */
  spi_tx[0] = ENABLE_INTERFACE_0;
  spi_tx[1] = ENABLE_INTERFACE_1;
  spi_tx[2] = ENABLE_INTERFACE_2;
  spi_tx[3] = ENABLE_INTERFACE_3;

  for(i = 0; i <= ZWAVE_SYNC_RETRY; i++)
  {
    /* Start SPI transer. */
    if (!spi_transfer(spi_tx, spi_rx, ZWAVE_MSG_LEN))
      return false;

    if ((spi_rx[2] == ENABLE_INTERFACE_1)
    &&  (spi_rx[3] == ENABLE_INTERFACE_2))
      break;
    else
    {
      /* If on the 33rd try, we are not fully synced, give up */
      if (i == ZWAVE_SYNC_RETRY)
      {
        fprintf(stderr, "Error: Z-Wave Programming Interface Could Not Sync\n");
        return false;
      }

      /* Pulse the SCLK line to progress syncing */
      zwave_reset_logic(SPI_CLK_PULSE);
    }
  }

  return true;
}

static bool command_successful(const uint8_t *p_spi_rx,
                               const uint8_t *p_spi_tx)
{
  return ((p_spi_rx[1] == p_spi_tx[0])  &&
          (p_spi_rx[2] == p_spi_tx[1]));
}

static bool zwave_check_state(zwave_state_t *zwave_state)
{
  uint8_t state;
  uint8_t spi_tx[ZWAVE_MSG_LEN];
  uint8_t spi_rx[ZWAVE_MSG_LEN];

  spi_tx[0] = CHECK_STATE_0;
  spi_tx[1] = CHECK_STATE_1;
  spi_tx[2] = CHECK_STATE_2;
  spi_tx[3] = CHECK_STATE_3;

  if (!spi_transfer(spi_tx, spi_rx, ZWAVE_MSG_LEN))
    return false;

  if (!command_successful(spi_rx, spi_tx))
  {
    fprintf(stderr, "Error: Didn't Receive Correct State Check Information\n");
    return false;
  }

  state = spi_rx[3];

  zwave_state->crc_busy        = (state & BIT(0)) != 0;
  zwave_state->crc_done        = (state & BIT(1)) != 0;
  zwave_state->crc_failed      = (state & BIT(2)) != 0;
  zwave_state->flash_fsm_busy  = (state & BIT(3)) != 0;
  zwave_state->cont_op_refused = (state & BIT(5)) != 0;

  return true;
}

static bool zwave_wait_flash_fsm(void)
{
  zwave_state_t  zwave_state;
  unsigned int   count = 0;

  for(;;)
  {
    zwave_check_state(&zwave_state);
    if (!zwave_state.flash_fsm_busy)
      return true;

    usleep(5000);
    if (count++ == 300)
    {
      fprintf(stderr, "Timeout waiting for flash FSM to clear\n");
      return false;
    }
  }
}

static bool pgm_command_successful(const uint8_t *p_spi_rx,
                                   const uint8_t *p_spi_tx)
{
  return ((p_spi_rx[1] == p_spi_tx[0])  &&
          (p_spi_rx[2] == p_spi_tx[1])  &&
          (p_spi_rx[3] == p_spi_tx[2]));
}

bool zwave_erase_flash_sectors(void)
{
  uint8_t  spi_tx[ZWAVE_MSG_LEN];
  uint8_t  spi_rx[ZWAVE_MSG_LEN];
  int      i;

  for(i = 0; i < ZWAVE_3502_SECTOR_CNT; i++)
  {
    spi_tx[0] = ERASE_SECT_0;
    spi_tx[1] = ERASE_SECT_1 | i;
    spi_tx[2] = ERASE_SECT_2;
    spi_tx[3] = ERASE_SECT_3;

    if (!spi_transfer(spi_tx, spi_rx, ZWAVE_MSG_LEN))
      return false;

    if (!pgm_command_successful(spi_rx, spi_tx))
    {
      fprintf(stderr, "Error Erasing Sector %d\n", i);
      return false;
    }

    if (!zwave_wait_flash_fsm())
      return false;
  }

  return true;
}

static bool zwave_check_crc32(void)
{
  zwave_state_t  zwave_state;
  uint8_t        spi_tx[ZWAVE_MSG_LEN];
  uint8_t        spi_rx[ZWAVE_MSG_LEN];

  spi_tx[0] = CHECK_CRC_0;
  spi_tx[1] = CHECK_CRC_1;
  spi_tx[2] = CHECK_CRC_2;
  spi_tx[3] = CHECK_CRC_3;

  if (!spi_transfer(spi_tx, spi_rx, ZWAVE_MSG_LEN))
    return false;

  if (!pgm_command_successful(spi_rx, spi_tx))
  {
    fprintf(stderr, "Error Running CRC Check\n");
    return false;
  }

  /* Wait while CRC calculates */
  do
  {
    usleep(5000);
    zwave_check_state(&zwave_state);
    // XXX: TODO -- add timeout counter here.
  } while (zwave_state.crc_busy);

  if (zwave_state.crc_failed)
  {
    fprintf(stderr, "Error: CRC Check Failed\n");
    return false;
  }
  else if (zwave_state.crc_done)
    return true;
  else
  {
    fprintf(stderr, "Error: Unexpected CRC Check Result\n");
    return false;
  }
}

static uint16_t find_start_addr(const uint8_t *p_data)
{
  uint16_t sram_start_addr = 0;

  while (p_data[sram_start_addr] == 0xFF)
    if (++sram_start_addr == (ZWAVE_3502_SRAM_SZ - 1))
      break;

  return sram_start_addr;
}

static uint16_t find_end_addr(const uint8_t *p_data)
{
  uint16_t sram_end_addr = ZWAVE_3502_SRAM_SZ - 1;

  while (p_data[sram_end_addr] == 0xFF)
    if (--sram_end_addr == 0)
      break;

  return sram_end_addr;
}

static bool write_one_byte(const uint8_t *p_data, uint16_t addr)
{
  uint8_t spi_tx[ZWAVE_MSG_LEN];
  uint8_t spi_rx[ZWAVE_MSG_LEN];

  spi_tx[0] = WRITE_SRAM_0;
  spi_tx[1] = WRITE_SRAM_1 | (uint8_t )(addr >> 8);
  spi_tx[2] = WRITE_SRAM_2 | (uint8_t )addr;
  spi_tx[3] = WRITE_SRAM_3 | p_data[addr];

  if (!spi_transfer(spi_tx, spi_rx, ZWAVE_MSG_LEN))
    return false;

  if (!pgm_command_successful(spi_rx, spi_tx))
  {
    fprintf(stderr,
            "Error Writing Address %u to SRAM\n", 
            (unsigned int )(addr - 1));
    return false;
  }

  return true;
}

static bool write_triples(const uint8_t *p_data,
                          uint16_t      *p_addr,
                          uint16_t       end)
{
  uint8_t   spi_tx[ZWAVE_MSG_LEN];
  uint8_t   spi_rx[ZWAVE_MSG_LEN];
  uint16_t  addr = *p_addr;

  while (addr < end)
  {
    spi_tx[0] = CONTINUE_WRITE_0;
    spi_tx[1] = CONTINUE_WRITE_1 | p_data[addr++];
    spi_tx[2] = CONTINUE_WRITE_2 | p_data[addr++];
    spi_tx[3] = CONTINUE_WRITE_3 | p_data[addr++];

    if (!spi_transfer(spi_tx, spi_rx, ZWAVE_MSG_LEN))
      return false;

    if (!pgm_command_successful(spi_rx, spi_tx))
    {
      fprintf(stderr, 
              "Error Continue Writing Data Near Address %u to SRAM\n", 
              (unsigned int )addr);
      return false;
    }
  }

  *p_addr = addr;
  return true;
}

static bool flash_sector(uint8_t sector)
{
  uint8_t   spi_tx[ZWAVE_MSG_LEN];
  uint8_t   spi_rx[ZWAVE_MSG_LEN];

  spi_tx[0] = WRITE_FLASH_SECT_0;
  spi_tx[1] = WRITE_FLASH_SECT_1 | sector;
  spi_tx[2] = WRITE_FLASH_SECT_2;
  spi_tx[3] = WRITE_FLASH_SECT_3;
      
  if (!spi_transfer(spi_tx, spi_rx, ZWAVE_MSG_LEN))
    return false;

  if (!pgm_command_successful(spi_rx, spi_tx))
  {
    fprintf(stderr,
            "Error Writing Flash Sector %u\n",
            (unsigned int )flash_sector);
    return false;
  }

  return zwave_wait_flash_fsm();
}

bool zwave_write_flash(const uint8_t *p_data, size_t data_len)
{
  uint8_t sector;

  if (!zwave_sync_programmer())
    return false;

  if (!zwave_erase_flash_sectors())
    return false;

  /* Write SRAM then write SRAM to flash. */
  for(sector = 0; sector < ZWAVE_3502_SECTOR_CNT; sector++)
  {
    uint16_t sram_start_addr;
    uint16_t sram_end_addr;
    uint16_t sram_write_addr;
    uint8_t  bytes_left;

    /* Ensure we have at least a sector left in the buffer. */
    if (data_len < ZWAVE_3502_SECTOR_SZ)
    {
      fprintf(stderr, "Data buffer too small to program\n");
      return false;
    }

    sram_start_addr = find_start_addr(p_data);
    sram_end_addr   = find_end_addr(p_data);

    /* Check if we even need to write this flash block. */
    if (sram_start_addr <= sram_end_addr)
    {
      sram_write_addr = sram_start_addr;

      if (!write_one_byte(p_data, sram_write_addr++))
        return false;

      /*
       * Determine how many bytes left that don't make up a 3 byte chunk.
       * We increment sram_end_addr to account for the 0th address when
       * moduloing.
       */
      ++sram_end_addr;
      bytes_left = (sram_end_addr - sram_write_addr) % 3;

      if (!write_triples(p_data, &sram_write_addr, sram_end_addr - bytes_left))
        return false;
      if (!flash_sector(sector))
        return false;

      /* Now we have to write the last 1 or 2 bytes to SRAM */
      while (bytes_left != 0)
      {
        if (!write_one_byte(p_data, sram_write_addr++))
          return false;
        if (!flash_sector(sector))
          return false;

        --bytes_left;
      }
    }

    /* Advance the buffer to the next sector. */    
    p_data   += ZWAVE_3502_SECTOR_SZ;
    data_len -= ZWAVE_3502_SECTOR_SZ;
  }
          
  if (!zwave_check_crc32())
    return false;

  return zwave_reset_logic(RESET_H);
}

static bool zwave_read_nvr(uint8_t *p_buf)
{
  uint8_t spi_tx[ZWAVE_MSG_LEN];
  uint8_t spi_rx[ZWAVE_MSG_LEN];
  int     i;

  for(i = 0; i < ZWAVE_3502_NVR_SZ; i++)
  {
    spi_tx[0] = READ_NVR_0;
    spi_tx[1] = READ_NVR_1;
    spi_tx[2] = READ_NVR_2 | (ZWAVE_3502_NVR_START_ADDR + i);
    spi_tx[3] = READ_NVR_3;

    if (!spi_transfer(spi_tx, spi_rx, ZWAVE_MSG_LEN))
      return false;

    if (!command_successful(spi_rx, spi_tx))
    {
      fprintf(stderr, 
              "Error Reading Back NVR Byte at address %X\n", 
              (unsigned int )(i + ZWAVE_3502_NVR_START_ADDR));
      return false;
    }

    p_buf[i] = spi_rx[3];
  }
  
  return true;
}

static bool zwave_erase_all_memory(void)
{
  uint8_t spi_tx[ZWAVE_MSG_LEN];
  uint8_t spi_rx[ZWAVE_MSG_LEN];

  spi_tx[0] = ERASE_CHIP_0;
  spi_tx[1] = ERASE_CHIP_1;
  spi_tx[2] = ERASE_CHIP_2;
  spi_tx[3] = ERASE_CHIP_3;

  if (!spi_transfer(spi_tx, spi_rx, ZWAVE_MSG_LEN))
    return false;

  if (!pgm_command_successful(spi_rx, spi_tx))
  {
    fprintf(stderr, "Error Erasing All Memory\n");
    return false;
  }

  return zwave_wait_flash_fsm();
}

static bool zwave_read_sram(uint8_t *p_result, uint16_t addr)
{
  uint8_t spi_tx[ZWAVE_MSG_LEN];
  uint8_t spi_rx[ZWAVE_MSG_LEN];

  spi_tx[0] = READ_SRAM_0;
  spi_tx[1] = READ_SRAM_1 | (uint8_t )(addr >> 8);
  spi_tx[2] = READ_SRAM_2 | (uint8_t )addr;
  spi_tx[3] = READ_SRAM_3;

  if (!spi_transfer(spi_tx, spi_rx, ZWAVE_MSG_LEN))
    return false;

  if (!command_successful(spi_rx, spi_tx))
  {
    fprintf(stderr,
            "Error Reading SRAM Byte at Address %04X\n",
            (unsigned int )addr);
    return false;
  }

  *p_result = spi_rx[3];
  return true;
}

static bool zwave_write_nvr(uint8_t data, uint8_t addr)
{
  uint8_t spi_tx[ZWAVE_MSG_LEN];
  uint8_t spi_rx[ZWAVE_MSG_LEN];

  spi_tx[0] = WRITE_NVR_0;
  spi_tx[1] = WRITE_NVR_1;
  spi_tx[2] = WRITE_NVR_2 | addr;
  spi_tx[3] = WRITE_NVR_3 | data;

  if (!spi_transfer(spi_tx, spi_rx, ZWAVE_MSG_LEN))
    return false;

  if (!pgm_command_successful(spi_rx, spi_tx))
  {
    fprintf(stderr, 
            "Error Writing NVR Byte at Address %X\n",
            (unsigned int )addr);
    return false;
  }

  return true;
}

bool zwave_calibrate_tx(const uint8_t *p_calibration_fw, size_t fw_len,
                        uint8_t *p_ccal)
{
  uint8_t  nvr[ZWAVE_3502_NVR_SZ];
  uint16_t crc;
  uint8_t  ccal, txcal1, txcal2;
  size_t   i;

  if (!zwave_sync_programmer())
    return false;

  /* Read the factory programmed data. */
  if (!zwave_read_nvr(nvr))
  {
    *p_ccal = 0xFF;
    return false;
  }

  /* Grab the factory crystal calibration. */
  *p_ccal = ccal = nvr[NVR_CCAL];
  if ((ccal == 0xFF) || (ccal == 0x80))
  {
    fprintf(stderr, "Invalid CCAL value %02X\n", (unsigned int )ccal);
    return false;
  }

  /* Erase All Memory */
  if (!zwave_erase_all_memory())
    return false;

  /* Write calibration file to flash */
  if (!zwave_write_flash(p_calibration_fw, fw_len))
    return false;

  /* Release reset and let calibration firmware run. */
  if (!zwave_reset_logic(RESET_H))
    return false;

  usleep(1300000);

  /* Go back into programming mode */
  if (!zwave_sync_programmer())
    return false;

  /* Grab txcal1 and txcal2 from SRAM */
  if (!zwave_read_sram(&txcal1, ZWAVE_3502_TXCAL1_ADDR))
    return false;
  if (!zwave_read_sram(&txcal2, ZWAVE_3502_TXCAL2_ADDR))
    return false;

  if ((txcal1 == 0xFF) || (txcal1 == 0x00))
  {
    fprintf(stderr, "Calibration failure %02x %02x\n",
            (unsigned int )txcal1,
            (unsigned int )txcal2);
    return false;
  }

  /* Write Back Values to NVR */
  zwave_write_nvr(NVR_RBAP_VAL,   NVR_RBAP); 
  zwave_write_nvr(NVR_REV_VAL,    NVR_REV); 
  zwave_write_nvr(ccal,           NVR_CCAL); 
  zwave_write_nvr(NVR_PINS_VAL,   NVR_PINS); 
  zwave_write_nvr(NVR_SAWC_VAL0,  NVR_SAWC + 0); 
  zwave_write_nvr(NVR_SAWC_VAL1,  NVR_SAWC + 1); 
  zwave_write_nvr(NVR_SAWC_VAL2,  NVR_SAWC + 2); 
  zwave_write_nvr(NVR_SAWB_VAL,   NVR_SAWB); 
  zwave_write_nvr(NVR_NVMT_VAL,   NVR_NVMT); 
  zwave_write_nvr(NVR_NVMCS_VAL,  NVR_NVMCS);
  zwave_write_nvr(NVR_NVMS_VAL0,  NVR_NVMS + 0);
  zwave_write_nvr(NVR_NVMS_VAL1,  NVR_NVMS + 1);
  zwave_write_nvr(NVR_NVMP_VAL0,  NVR_NVMP + 0);
  zwave_write_nvr(NVR_NVMP_VAL1,  NVR_NVMP + 1);
  zwave_write_nvr(txcal1,         NVR_TXCAL1); 
  zwave_write_nvr(txcal2,         NVR_TXCAL2); 
  
  if (!zwave_read_nvr(nvr))
    return false;

  /* Calculate CRC16. */
  crc = ZWAVE_3502_CRC_INIT_VAL;
  for(i = ZWAVE_3502_CRC_START_ADDR; i <= ZWAVE_3502_CRC_END_ADDR; i++)
    crc = update_crc_ccitt(crc, (char )nvr[i]);

  /* Write the CRC16 to NVR */
  zwave_write_nvr((uint8_t )(crc >> 8), NVR_CRC16 + 0); 
  zwave_write_nvr((uint8_t )crc,        NVR_CRC16 + 1); 

  usleep(5000);
  return true;
}

/****************************************************************************/
/*                           MANUFACTURE TEST                               */
/****************************************************************************/

static char *find_delimiter(char *p_buffer, size_t len)
{
  char    *p_found;
  size_t   offs; 

  for(;;)
  {
    p_found = memchr(p_buffer, '-', len);
    if (p_found == NULL)
    {
      p_found = memchr(p_buffer, '+', len);
      if (p_found == NULL)
        return (NULL);
    }

    offs = (size_t )(p_found - p_buffer);
    if (offs == len)
      return (NULL);

    if (isxdigit(p_found[1]))
      return (p_found);
 
    p_buffer  = p_found + 1;
    len      -= offs;
  }
}

bool zwave_test_peer(unsigned int *p_count)
{
  char            buf[1024];
  size_t          pos = 0;
  unsigned int    timeout = 5000;

  /* Start the testing procedure. */
  if (!zwave_reset_logic(RESET_L))
    return false;
  usleep(10000UL);
  if (!zwave_uart_flush_rx())
    return false;
  if (!zwave_reset_logic(RESET_H))
    return false;

  /* Wait for the result. */
  while (pos < sizeof(buf))
  {
    char    *p_found;
    size_t   delim;

    /* Read a byte into the buffer. */
    if (!zwave_uart_read_timeout((uint8_t *)buf + pos, 1, &timeout))
      return false;
    else
      pos++;

    /* If there is no delimiter in the buffer yet then keep reading. */
    p_found = find_delimiter(buf, pos);
    if (p_found == NULL)
      continue;
    
    /* Ensure the delimiter is followed by 4 characters. */
    delim = (size_t )(p_found - buf);
    if ((delim + 5) <= pos)
    {
      char hex[5];

      /* Return the numeric 16-bit value and conclude the test. */
      memcpy(hex, p_found + 1, 4);
      hex[4] = '\0';
      *p_count = (unsigned int )strtoul(hex, NULL, 16);
      return true;
    }
  }

  return false;
}

/* 
 * zwave_read_ext_nvm & zwave_write_ext_nvm is specific 
 * to the hardware on the CliqHost/Express1.
 *
 * Serial API:        : 6.51.03
 * Z-Wave Module      : ZM5202
 * External NVM Eeprom: CAT25256
 * Size               : 32K bytes (32768 bytes)
 * Page Size          : 64  bytes  
 */
#define ZWAVE_EEPROM_BACKUP   "/tests/zwave_fw/eeprom_backup.bin"
#define CAT25256_SZ           32768

#define CAT25256_R_PG_SZ      128
#define CAT25256_R_PG_NUMB    256

#define CAT25256_W_PG_SZ      64
#define CAT25256_W_PG_NUMB    512
bool zwave_read_ext_nvm(void)
{
  /* Rx will be 5 API bytes before the actual EEPROM data
   * followed by 1 checksum byte at the end. The 5 at the 
   * beginning are ACK, SOF, Len, Type, ID. 
   */
  static uint8_t  nvm[CAT25256_SZ];
  uint8_t         rx[CAT25256_R_PG_SZ + 6];
  uint8_t         tx[9]; 
  int             fd, i;
  unsigned int    to = 10000;

  tx[0] = 0x01;                             /* SOF */
  tx[1] = 0x08;                             /* 8 bytes not including SOF */
  tx[2] = 0x01;                             /* Command type: RES */ 
  tx[3] = 0x2A;                             /* Command ID */
  tx[4] = 0x00;                             /* Offset addr MSB */
  tx[5] = 0x00;                             /* Offset addr     */
  tx[6] = 0x00;                             /* Offset addr LSB */
  tx[7] = (uint8_t)(CAT25256_R_PG_SZ >> 8); /* Length MSB */
  tx[8] = (uint8_t)(CAT25256_R_PG_SZ);      /* Length LSB */ 

  for (i = 0; i < CAT25256_R_PG_NUMB; i++)
  {
    tx[5] = (uint8_t )((i * CAT25256_R_PG_SZ) >> 8); 
    tx[6] = (uint8_t )(i * CAT25256_R_PG_SZ);

    if (!zwave_uart_write(tx, sizeof(tx)))
      return false;
    
    if (!zwave_uart_read_timeout(rx, sizeof(rx), &to))
      return false;

    if (!zwave_uart_ack())
      return false;

    if (!check_response(rx, sizeof(rx)))
      return false;
    
    memcpy(&nvm[i*CAT25256_R_PG_SZ], &rx[5], CAT25256_R_PG_SZ);
  }

  /* Lock file and write the buffer contents to file */
  fd = open(ZWAVE_EEPROM_BACKUP, O_RDWR | O_CREAT, 0666);
  if (fd < 0)
  {
    fprintf(stderr, "Can't open %s\n", ZWAVE_EEPROM_BACKUP);
    return false;
  }
  else 
  {
    struct flock flock;

    flock.l_type   = F_WRLCK; 
    flock.l_whence = SEEK_SET;
    flock.l_start  = 0;
    flock.l_len    = 0;

    if (fcntl(fd, F_SETLKW, &flock) != 0)
    {
      fprintf(stderr, "Can't lock %s for writing\n", ZWAVE_EEPROM_BACKUP);
      close(fd);
      return false;
    }
    else
    {
      if (write(fd, nvm, CAT25256_SZ) != CAT25256_SZ)
      {
        fprintf(stderr, "Can't write to %s\n", ZWAVE_EEPROM_BACKUP);
        close(fd);
        return false;
      }
    }

    close(fd);
  }

  return true;
}

bool zwave_write_ext_nvm(const uint8_t *p_data, size_t data_len)
{
  /* Rx includes ACK, SOF, Len, Type, ID, retval, and checksum */
  uint8_t         tx[CAT25256_W_PG_SZ + 9]; 
  uint8_t         rx[7]; 
  int             i, j;
  unsigned int    to = 50000;

  if (data_len < CAT25256_SZ)
  {
    fprintf(stderr, "Data buffer too small to program\n");
    return false;
  }
  if (data_len > CAT25256_SZ)
  {
    fprintf(stderr, "Data buffer too large to program\n");
    return false;
  }
    
  tx[0] = 0x01;                                /* SOF */
  tx[1] = 8+64;                                /* 8 bytes sans SOF + NVM data */
  tx[2] = 0x00;                                /* Command type: REQ */ 
  tx[3] = 0x2B;                                /* Command ID */
  tx[4] = 0x00;                                /* Offset addr MSB */
  tx[5] = 0x00;                                /* Offset addr     */
  tx[6] = 0x00;                                /* Offset addr LSB */
  tx[7] = (uint8_t )(CAT25256_W_PG_SZ >> 8);   /* Length MSB */
  tx[8] = (uint8_t )(CAT25256_W_PG_SZ);        /* Length LSB */ 

  for (i = 0; i < CAT25256_W_PG_NUMB; i++)
  {
    tx[5] = (uint8_t )((i * CAT25256_W_PG_SZ) >> 8); 
    tx[6] = (uint8_t )(i * CAT25256_W_PG_SZ); 

    /* Copy from file pointer data to local tx buffer */
    for (j = 0; j < CAT25256_W_PG_SZ; j++)
      tx[9 + j] = *p_data++;

    if (!zwave_uart_write(tx, sizeof(tx)))
    {
      fprintf(stderr, "Failed zwave_uart_write\n");
      return false;
    }
    
    if (!zwave_uart_read_timeout(rx, sizeof(rx), &to))
    {
      fprintf(stderr, "Failed uart read timeout\n");
      return false;
    }

    if (!zwave_uart_ack())
    {
      fprintf(stderr, "Failed uart ack\n"); 
      return false;
    }

    /* Check the retval from the EEPROM write cmd response */
    if ( rx[5] != true )
    {
      fprintf(stderr, "Z-Wave API indicates write did not take place\n");
      return false;
    }

    if (!check_response(rx, sizeof(rx)))
    {
      fprintf(stderr, "Failed check response\n");
      return false;
    }
  }

  //fprintf(stderr, "We have successful programmed the Eeprom.\n");
  return true;
}

/* This is "ZW_Version" in the API */
bool zwave_get_api(char *api)
{
  uint8_t      tx[4];
  uint8_t      rx[19];
  int          i;
  unsigned int to = 1000;

  tx[0] = 0x01;               /* SOF                       */
  tx[1] = 0x03;               /* 8 bytes not including SOF */
  tx[2] = 0x00;               /* Command type: REQ         */ 
  tx[3] = 0x15;               /* Command ID                */

  if (!zwave_uart_write(tx, sizeof(tx)))
    return false;

  if (!zwave_uart_read_timeout(rx, sizeof(rx), &to))
    return false;

  if (!zwave_uart_ack())
    return false;

  if (!check_response(rx, sizeof(rx)))
    return false;

  /* The API string length is 12 and starts at the 6th byte in the Rx buffer*/
  for (i = 0; i < 12; i++)
  {
    api[i] = (char )rx[5+i];
  }

  return true;
}

bool zwave_set_rfpower(uint8_t *power)
{
  /* Rx includes ACK, SOF, Len, Type, ID, retval, and checksum */
  uint8_t       tx[5];
  uint8_t       rx[7];
  unsigned int  to = 2000;

  tx[0] = 0x01;               /* SOF                       */
  tx[1] = 0x04;               /* 4 bytes not including SOF */
  tx[2] = 0x00;               /* Command type: REQ         */ 
  tx[3] = 0x17;               /* Command ID                */
  tx[4] = *power;             /* Power Level               */

  if (!zwave_uart_write(tx, sizeof(tx)))
    return false;

  if (!zwave_uart_read_timeout(rx, sizeof(rx), &to))
    return false;

  if (!zwave_uart_ack())
    return false;

  if (!check_response(rx, sizeof(rx)))
    return false;

  /* Grab the 6th Rx byte which is the power level return value. */
  *power = rx[5];

  return true;
}

bool zwave_get_rfpower(uint8_t *power)
{
  /* Rx includes ACK, SOF, Len, Type, ID, retval, and checksum */
  uint8_t       tx[4];
  uint8_t       rx[7];
  unsigned int  to = 2000;

  tx[0] = 0x01;               /* SOF                       */
  tx[1] = 0x03;               /* 3 bytes not including SOF */
  tx[2] = 0x00;               /* Command type: REQ         */ 
  tx[3] = 0xBA;               /* Command ID                */

  if (!zwave_uart_write(tx, sizeof(tx)))
    return false;

  if (!zwave_uart_read_timeout(rx, sizeof(rx), &to))
    return false;

  if (!zwave_uart_ack())
    return false;

  if (!check_response(rx, sizeof(rx)))
    return false;

  /* Grab the 6th Rx byte which is the power level return value. */
  *power = rx[5];

  return true;
}

/**
  * This resets the Z-Wave controller to factory default, erasing all routing
  * information and choosing a new Home ID.
  */
bool zwave_factory_default(void)
{
  /* Rx includes ACK, SOF, Len, RES, ID, funcID, Checksum */
  uint8_t       tx[5];
  uint8_t       rx[7];
  unsigned int  to = 2000;
 
  tx[0] = 0x01;               /* SOF                       */
  tx[1] = 0x04;               /* 8 bytes not including SOF */
  tx[2] = 0x00;               /* Command type: REQ         */ 
  tx[3] = 0x42;               /* Command ID                */
  tx[4] = 0x69;               /* funcID                    */

  if (!zwave_uart_write(tx, sizeof(tx)))
    return false;

  if (!zwave_uart_read_timeout(rx, sizeof(rx), &to))
    return false;

  if (!zwave_uart_ack())
    return false;

  if (!check_response(rx, sizeof(rx)))
    return false;

  if (rx[5] != tx[4])
  {
    fprintf(stderr, "Factory default got invalid response\n");
    return false;
  }
    
  return true;
}

bool zwave_get_homeid(ZWAVE_HOME_ID *p_home_id)
{
  static const uint8_t tx[] = { 0x01, 0x03, 0x00, 0x20 };
  uint8_t              rx[11];
  unsigned int         to = 1000;
  ZWAVE_HOME_ID        id;

  if (!zwave_uart_write(tx, sizeof(tx)))
    return false;

  if (!zwave_uart_read_timeout(rx, sizeof(rx), &to))
    return false;

  if (!zwave_uart_ack())
    return false;

  if (!check_response(rx, sizeof(rx)))
    return false;

  id  = (ZWAVE_HOME_ID )rx[5] << 24;
  id |= (ZWAVE_HOME_ID )rx[6] << 16;
  id |= (ZWAVE_HOME_ID )rx[7] << 8;
  id |= (ZWAVE_HOME_ID )rx[8];   

  if (id < ZWAVE_RANDOM_HOMEID_START_RANGE)
  {
    fprintf(stderr,
            "Home ID (%lx) is too low to be valid\n",
            (unsigned long )id);
    return false;
  }

  if (id > ZWAVE_RANDOM_HOMEID_END_RANGE)
  {
    fprintf(stderr,
            "Home ID (%lx) is too high to be valid\n",
            (unsigned long )id);
    return false;
  }

  *p_home_id = id;
  return true;
}


