/*---------------------------------------------------------------------------
**
** MODULE NAME:     zwave_tool.c
**
** COPYRIGHT:       Copyright (c) 2014, Clare Controls, Inc.
**                  All Rights Reserved.
**
**                  NOTICE: THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF
**                  CLARE CONTROLS, INC., AND SHALL NOT BE REPRODUCED, COPIED
**                  IN WHOLE OR IN PART ADAPTED, MODIFIED, OR
**                  DISSEMINATED WITHOUT THE EXPRESS WRITTEN CONSENT
**                  OF CLARE CONTROLS, INC. ANY REDISTRIBUTION OR
**                  PUBLICATION TO UNAUTHORIZED PERSONS IS STRICTLY
**                  PROHIBITED BY U.S. AND INTERNATIONAL COPYRIGHT LAW.
**
** DESCRIPTION:     CLI wrapper for the Zwave API.
**
** AUTHOR:          M. Grosberg / G. Tozzi
**
** CREATION DATE:   Jul  9, 2014
**
**---------------------------------------------------------------------------
**
** HISTORY OF CHANGE  
**
** -----+--------+------------------------------------------+------+---------
** VERS | DATE   | DESCRIPTION OF CHANGE                    | INIT | PR#
** =====+========+==========================================+======+=========
**  001 |07/09/14| File creation.                           | MYG  |
** -----+--------+------------------------------------------+------+---------
**      |11/14/14| Added new functionality.                 | GVT  |
** -----+--------+------------------------------------------+------+---------
**      |12/07/15| Fixed WriteEeprom login issue.           | DKB  |
** -----+--------+------------------------------------------+------+---------
**-------------------------------------------------------------------------*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "zwave_api.h"

typedef struct
{
  unsigned char *data;
  size_t         len;
} FILE_DATA;

static void FreeFile(FILE_DATA *p_data)
{
  free(p_data->data);
}

static int OpenFile(const char *p_filename, FILE_DATA *p_data)
{
  int          fd;
  struct stat  sb;

  fd = open(p_filename, O_RDONLY);
  if (fd < 0)
  {
    fprintf(stderr, "Can't open %s: %s\n", p_filename, strerror(errno));
    return (EXIT_FAILURE);
  }

  if (fstat(fd, &sb) != 0)
  {
    fprintf(stderr, "Can't stat %s: %s\n", p_filename, strerror(errno));
    close(fd);
    return (EXIT_FAILURE);
  }

  p_data->len  = (size_t )sb.st_size;
  p_data->data = malloc(p_data->len);
  if (p_data->data == NULL)
  {
    fprintf(stderr, "Out of memory reading %s\n", p_filename);
    close(fd);
    return (EXIT_FAILURE);
  }

  if (read(fd, p_data->data, p_data->len) != (ssize_t )p_data->len)
  {
    fprintf(stderr, "Error reading file %s: %s\n", p_filename, strerror(errno));
    close(fd);
    return (EXIT_FAILURE);
  }

  close(fd);
  return (EXIT_SUCCESS);
}

/***************************************************************************/

static int DoProgram(int argc, const char **argv)
{
  int        rc;
  FILE_DATA  fd;

  if (argc < 1)
  {
    fprintf(stderr, "Missing filename\n");
    return (EXIT_FAILURE);
  }

  rc = OpenFile(argv[0], &fd);
  if (rc != EXIT_SUCCESS)
    return (rc);

  rc = zwave_write_flash(fd.data, fd.len) ? EXIT_SUCCESS : EXIT_FAILURE;
  FreeFile(&fd);
  return (rc);
}

static int DoCalibrate(int argc, const char **argv)
{
  int        rc;
  FILE_DATA  cal_fw;
  FILE_DATA  run_fw;
  uint8_t    ccal;
  
  if (argc < 2)
  {
    fprintf(stderr, "Missing calibration and firmware file names\n");
    return (EXIT_FAILURE);
  }

  rc = OpenFile(argv[0], &cal_fw);
  if (rc != EXIT_SUCCESS)
    return (rc);
  rc = OpenFile(argv[1], &run_fw);
  if (rc != EXIT_SUCCESS)
  {
    FreeFile(&cal_fw);
    return (rc);
  }

  if (zwave_calibrate_tx(cal_fw.data, cal_fw.len, &ccal))
  {
    if (!zwave_write_flash(run_fw.data, run_fw.len))
      rc = EXIT_FAILURE;
  }
  else
    rc = EXIT_FAILURE;

  printf(">>> CCAL = %02X\n", (unsigned int )ccal);

  FreeFile(&cal_fw);
  FreeFile(&run_fw);
  return (rc);
}

static int GetHomeId(void)
{
  ZWAVE_HOME_ID  id;

  if (!zwave_get_homeid(&id))
    return (EXIT_FAILURE);

  printf("ID = %lX\n", (unsigned long )id);
  return (EXIT_SUCCESS);
}

static int TestPeer(void)
{
  unsigned int  count;

  if (!zwave_test_peer(&count))
    return (EXIT_FAILURE);

  printf("COUNT = %u\n", count);
  return ((count < 5) ? EXIT_FAILURE : EXIT_SUCCESS);
}

static int ReadEeprom(void)
{
  if (!zwave_read_ext_nvm())
    return (EXIT_FAILURE);

  return (EXIT_SUCCESS);
}

static int WriteEeprom(int argc, const char **argv)
{
  int        rc;
  FILE_DATA  fd;
  
  if (argc < 1)
  {
    fprintf(stderr, "Missing file name\n");
    return (EXIT_FAILURE);
  }

  rc = OpenFile(argv[0], &fd);
  if (rc != EXIT_SUCCESS)
  {
    fprintf(stderr, "Failed to open file\n");
    return (rc);
  }

  if (!zwave_write_ext_nvm(fd.data, fd.len))
  {
    fprintf(stderr, "Failed to write zwave eeprom\n");
    rc = EXIT_FAILURE;
  }

  FreeFile(&fd);
  return (rc);
}

static int FactoryDefault(void)
{
  if (!zwave_factory_default())
    return (EXIT_FAILURE);

  return (EXIT_SUCCESS);
}

static int GetApi(void)
{
  int i;
  char api[12];

  if (!zwave_get_api(api))
    return (EXIT_FAILURE);

  for (i = 0; i < sizeof(api); i++)
    printf("%c", api[i]);

  printf("\n");

  return (EXIT_SUCCESS);
}

static int SetRfPowerLevel(int argc, const char **argv)
{
  uint8_t  power_level;
  char    *p;

  if (argc < 1)
  {
    fprintf(stderr, "Missing dB power level to reduce\n");
    return (EXIT_FAILURE);
  }

  /* Convert string to hex */
  power_level = (uint8_t )strtoul(argv[0], &p, 10);

  if (power_level > 9)
  {
    fprintf(stderr, "Reduce gain value. Max gain reduction is 9 dB\n");
    return (EXIT_FAILURE);
  }

  if (!zwave_set_rfpower(&power_level))
    return (EXIT_FAILURE);
  
  if (power_level != 0)
    printf("RF power level set to -%d dB\n", power_level);
  else
    printf("RF power level set to %d dB\n", power_level);

  return (EXIT_SUCCESS);
}

static int GetRfPowerLevel(void)
{
  uint8_t power_level;

  if (!zwave_get_rfpower(&power_level))
    return (EXIT_FAILURE);

  if (power_level != 0)
    printf("RF power level is -%d dB\n", power_level);
  else
    printf("RF power level is %d dB\n", power_level);

  return (EXIT_SUCCESS);
}

int main(int argc, const char **argv)
{
  int  rc;

  if (argc < 2)
  {
    fprintf(stderr, "Missing verb\n");
    return (EXIT_FAILURE);
  }

  if (!zwave_open())
    return (EXIT_FAILURE);

  if (strcmp(argv[1], "program-flash") == 0)
    rc = DoProgram(argc - 2, argv + 2);
  else if (strcmp(argv[1], "calibrate-tx") == 0)
    rc = DoCalibrate(argc - 2, argv + 2);
  else if (strcmp(argv[1], "get-home-id") == 0)
    rc = GetHomeId();
  else if (strcmp(argv[1], "test-peer") == 0)
    rc = TestPeer();
  else if (strcmp(argv[1], "eeprom-read") == 0)
    rc = ReadEeprom();
  else if (strcmp(argv[1], "eeprom-write") == 0)
    rc = WriteEeprom(argc - 2, argv + 2);
  //TODO:
  //else if (strcmp(argv[1], "eeprom-restore") == 0)
    //rc = RestoreEeprom(argc - 2, argv + 2);
  else if (strcmp(argv[1], "factory-default") == 0)
    rc = FactoryDefault();
  else if (strcmp(argv[1], "get-api") == 0)
    rc = GetApi();
  else if (strcmp(argv[1], "set-power") == 0)
    rc = SetRfPowerLevel(argc -2, argv + 2);
  else if (strcmp(argv[1], "get-power") == 0)
    rc = GetRfPowerLevel();
  else
  {
    fprintf(stderr, "Unknown verb: %s\n", argv[1]);
    rc = EXIT_FAILURE;
  }

  zwave_close();
  return (rc);
}
