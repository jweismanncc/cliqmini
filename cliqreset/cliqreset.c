/*---------------------------------------------------------------------------
**
** MODULE NAME:     clarereset.c
**
** COPYRIGHT:       Copyright (c) 2015, Clare Controls, Inc.
**                  All Rights Reserved.
**
**                  NOTICE: THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF
**                  CLARE CONTROLS, INC., AND SHALL NOT BE REPRODUCED, COPIED
**                  IN WHOLE OR IN PART ADAPTED, MODIFIED, OR
**                  DISSEMINATED WITHOUT THE EXPRESS WRITTEN CONSENT
**                  OF CLARE CONTROLS, INC. ANY REDISTRIBUTION OR
**                  PUBLICATION TO UNAUTHORIZED PERSONS IS STRICTLY
**                  PROHIBITED BY U.S. AND INTERNATIONAL COPYRIGHT LAW.
**
** DESCRIPTION:     Reset switch watching daemon.
**
** AUTHOR:          D. Budinsky
**
** CREATION DATE:   Nov 30, 2015
**
**---------------------------------------------------------------------------
**
** HISTORY OF CHANGE  
**
** -----+--------+------------------------------------------+------+---------
** VERS | DATE   | DESCRIPTION OF CHANGE                    | INIT | PR#
** =====+========+==========================================+======+=========
**  001 |11/20/15| File creation.                           | DKB  |
** -----+--------+------------------------------------------+------+---------
** -----+--------+------------------------------------------+------+---------
**-------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <inttypes.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>
#include <wiringPi.h>
#include <fcntl.h>
#include "../leddaemon/led-daemon.h"

// Pin definition
#define RESET_PIN 3

// High/Low definition
#define LOW 0
#define HIGH 1

// Time definition
#define MIN_TIME 3
#define MAX_TIME 10

/*---------------------------------------------------------------------------
**   
** NAME:           writeEventState
**
** DESCRIPTION:    Write the boolean status of event to the LED event list.
**   
** RETURNS:        POSIX status code.
**   
**------------------------------------------------------------------------*/
static int writeEventState(int event_addr)
{
  int rc = EXIT_SUCCESS;
  LED_EVENT_CONFIG event = {FALSE};

  event.state = TRUE;

  int fd = open(LED_EVENT_LIST, O_RDWR | O_CREAT, 0666);
  if (fd < 0)
  {
    fprintf(stderr, "Can't open for writing LED event list %s: %s", 
            LED_EVENT_LIST, strerror(errno));  
    rc = EXIT_FAILURE;
  }
  else
  {
    struct flock flock;
   
    flock.l_type = F_WRLCK;
    flock.l_whence = SEEK_SET;
    flock.l_start = 0;
    flock.l_len = 0;

    if (fcntl(fd, F_SETLKW, &flock) != 0)
    {
      fprintf(stderr, "Can't lock LED event list %s: %s", 
              LED_EVENT_LIST, strerror(errno));  
      rc = EXIT_FAILURE;
    }
    else
    {
      //TODO: event_addr is an int when lseek is expecting off_t
      if (lseek(fd, event_addr, SEEK_SET) != event_addr)
      {
        fprintf(stderr, "Can't seek LED event list %s: %s", 
                LED_EVENT_LIST, strerror(errno));  
        rc = EXIT_FAILURE;
      } 
      else
      {
        if (write(fd, &event, sizeof(event)) != sizeof(event))
        {
          fprintf(stderr, "Can't write LED event list %s: %s", 
                  LED_EVENT_LIST, strerror(errno));  
          rc = EXIT_FAILURE;
        }
      }
    } 
    
    close(fd);
  }

  return (rc);
}

/*---------------------------------------------------------------------------
**
** NAME:           Daemonize
**
** DESCRIPTION:    POSIX daemon startup routine.
**
** AUTHOR:         D. Budinsky
** CREATED:        Nov 30, 2015
** LAST MODIFIED:  N/A           BY:  N/A
**
**------------------------------------------------------------------------*/
static void Daemonize()
{
	pid_t pid;

	/* Fork off the parent process */
	pid = fork();

	/* An error occured */
	if (pid < 0)
	{
		exit(EXIT_FAILURE);
	}

	/* Success: Let the parent terminate */
	if (pid > 0)
	{
		exit(EXIT_SUCCESS);	
	}

	/* On success: The child process becomes session leader */
	if (setsid() < 0)
	{
		exit(EXIT_FAILURE);
	}

	/* CATCH, IGNORE AND HANDLE SIGNALS */
	//TODO: Implement a working signal handler */
	signal(SIGCHLD, SIG_IGN);
	signal(SIGHUP, SIG_IGN);

	/* Fork off for the second time */
	pid = fork();

	/* An error occured */
	if (pid < 0)
	{
		exit(EXIT_FAILURE);
	}

	/* Success: Let the parent terminate */
	if (pid > 0)
	{
		exit(EXIT_SUCCESS);
	}

	/* Set new file permissions */
	umask(0);

	/* Change the working directory to the root directory */
	/* or another appropriate directory */
	chdir("/");

	/* Close all open file descriptors */
	int x;
	for (x = sysconf(_SC_OPEN_MAX); x > 0; x--)
	{
		close (x);
	}

	/* Open the file log */
	openlog ("cliqdaemon", LOG_PID, LOG_DAEMON);
}

/*---------------------------------------------------------------------------
**
** NAME:           ResetRebootCliq
**
** DESCRIPTION:    method used to reboot and clean up WPA information
**		based on the boolean value passed in.
**
** PARAMETER: bool reset - delete the wpa information if set to true.
**
** AUTHOR:         D. Budinsky
** CREATED:        Nov 30, 2015
** LAST MODIFIED:  N/A           BY:  N/A
**
**------------------------------------------------------------------------*/
void ResetRebootCliq(bool reset)
{

	writeEventState(SHUTDOWN_ADDR);

	sleep(2);

	if (reset)
	{
		syslog(LOG_NOTICE, "Removing WPA Information");
		remove("/usr/local/clarecontrols/wireless/wpa_supplicant.conf");

		sync();

		if (execl("/sbin/shutdown", "shutdown", "-r", "now", NULL) == -1 )
		{
			syslog(LOG_ERR, "'shutdown' program failed to run with error: %d", errno);
		}

	}
	else
	{

		syslog(LOG_NOTICE, "Manual Shutdown Issued - Shutting system now!!!");
		sync();

		if (execl("/sbin/shutdown", "shutdown", "-h", "now", NULL) == -1 )
		{
			syslog(LOG_ERR, "'shutdown' program failed to run with error: %d", errno);
		}
	}	
}

/*---------------------------------------------------------------------------
**
** NAME:           main
**
** DESCRIPTION:    Main entry point.
**
** AUTHOR:         D. Budinsky
** CREATED:        Nov 30, 2015
** LAST MODIFIED:  N/A           BY:  N/A
**
**------------------------------------------------------------------------*/
int main()
{
	long	totaltime = 0, starttime = 0;
	long	ms; // milliseconds
	time_t	s;  // seconds
	struct timespec spec;

	Daemonize();

	if (wiringPiSetup () == -1)
	{
		syslog (LOG_NOTICE, "Failed to load wiringPi library");
		return 1;
	}

	// Setup our gpio pin to be input
	pinMode (RESET_PIN, INPUT);

	syslog (LOG_NOTICE, "cliqreset daemon started.");

	while (1)
	{
		if ( digitalRead(RESET_PIN) )	// Buton is released if this returns 1
		{
			if ( totaltime > MIN_TIME && totaltime  < MAX_TIME )
			{
				// We are in a state where we need to reset the mini
				syslog(LOG_NOTICE, "cliqreset - shutting down system" );

				ResetRebootCliq(false);
			}
			else if ( totaltime > MAX_TIME )
			{
				// Now we remove the wireless wpa information, and reset the mini
				syslog(LOG_NOTICE, "cliqreset - reset WPA Information");

				ResetRebootCliq(true);
			}
			// reset the time variables both to 0.
			totaltime = 0;
			starttime = 0;
		}
		else							// If digitalRead returns 0, button is pushed.
		{

			// Get the current time using MONOTONIC clock
			clock_gettime(CLOCK_MONOTONIC, &spec);

			if ( starttime == 0 )
			{
				// We need to set our start time for the press
				starttime =  spec.tv_sec;
			}
			totaltime = spec.tv_sec - starttime;
		}
		usleep(240000);
	}

	syslog (LOG_NOTICE, "cliqreset terminated.");
	closelog();

	return EXIT_SUCCESS;
}
