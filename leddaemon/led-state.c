/*---------------------------------------------------------------------------
**
** MODULE NAME:     led-status.c
**
** COPYRIGHT:       Copyright (c) 2014, Clare Controls, Inc.
**                  All Rights Reserved.
**
**                  NOTICE: THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF
**                  CLARE CONTROLS, INC., AND SHALL NOT BE REPRODUCED, COPIED
**                  IN WHOLE OR IN PART ADAPTED, MODIFIED, OR
**                  DISSEMINATED WITHOUT THE EXPRESS WRITTEN CONSENT
**                  OF CLARE CONTROLS, INC. ANY REDISTRIBUTION OR
**                  PUBLICATION TO UNAUTHORIZED PERSONS IS STRICTLY
**                  PROHIBITED BY U.S. AND INTERNATIONAL COPYRIGHT LAW.
**
** DESCRIPTION:     LED event command line tool. Derived from previous LED
**						tool.
**
** AUTHOR:          Doug Budinsky
**
** CREATION DATE:   Dec 15, 2015
**
**---------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <syslog.h>
#include "led-daemon.h"

/*---------------------------------------------------------------------------
**
** NAME:           writeEventState
**
** DESCRIPTION:    Write the boolean status of event to the LED event list.
**
** RETURNS:        POSIX status code.
**
**------------------------------------------------------------------------*/
static int writeEventState(int event_addr, const char *argv[])
{
  int rc = EXIT_SUCCESS;
  LED_EVENT_CONFIG event = {FALSE};

  if (strcmp(argv[0], "false") == 0)
    event.state = FALSE;
  else if (strcmp(argv[0], "true") == 0)
    event.state = TRUE;
  else
  {
    fprintf(stderr, "Unknown state %s\n", argv[0]);
    rc = EXIT_FAILURE;
  }

  int fd = open(LED_EVENT_LIST, O_RDWR | O_CREAT, 0666);
  if (fd < 0)
  {
    fprintf(stderr, "Can't open for writing LED event list %s: %s", 
            LED_EVENT_LIST, strerror(errno));  
    rc = EXIT_FAILURE;
  }
  else
  {
    struct flock flock;
   
    flock.l_type = F_WRLCK;
    flock.l_whence = SEEK_SET;
    flock.l_start = 0;
    flock.l_len = 0;

    if (fcntl(fd, F_SETLKW, &flock) != 0)
    {
      fprintf(stderr, "Can't lock LED event list %s: %s", 
              LED_EVENT_LIST, strerror(errno));  
      rc = EXIT_FAILURE;
    }
    else
    {
      //TODO: event_addr is an int when lseek is expecting off_t
      if (lseek(fd, event_addr, SEEK_SET) != event_addr)
      {
        fprintf(stderr, "Can't seek LED event list %s: %s", 
                LED_EVENT_LIST, strerror(errno));  
        rc = EXIT_FAILURE;
      } 
      else
      {
        if (write(fd, &event, sizeof(event)) != sizeof(event))
        {
          fprintf(stderr, "Can't write LED event list %s: %s", 
                  LED_EVENT_LIST, strerror(errno));  
          rc = EXIT_FAILURE;
        }
      }
    } 
    
    close(fd);
  }

  return (rc);
}

/*---------------------------------------------------------------------------
**
** NAME:           HandleCommand
**
** DESCRIPTION:    Parse the arguments and execute the appropriate action.
**
** RETURNS:        POSIX status code.
**
**------------------------------------------------------------------------*/
static int handleCommand(int argc, const char *argv[])
{
  	if (strcmp(argv[0], "clarehome") == 0)
    	return (writeEventState(CLAREHOME_ADDR, argv+1));
  	else if (strcmp(argv[0], "boot") == 0)
    	return (writeEventState(BOOT_ADDR, argv+1));
  	else if (strcmp(argv[0], "ethernet") == 0)
    	return (writeEventState(ETHERNET_ADDR, argv+1));
  	else if (strcmp(argv[0], "apmode") == 0)
    	return (writeEventState(APMODE_ADDR, argv+1));
  	else if (strcmp(argv[0], "shutdown") == 0)
    	return (writeEventState(SHUTDOWN_ADDR, argv+1));
	else if (strcmp(argv[0], "nodns") == 0)
		return (writeEventState(NODNS_ADDR, argv+1));
	else if (strcmp(argv[0], "update") == 0)
		return (writeEventState(UPDATE_ADDR, argv+1));
  	else
  	{
    	fprintf(stderr, "Unknown command: %s\n", argv[0]);
    	return (EXIT_FAILURE);
  	}
}

/*---------------------------------------------------------------------------
**
** NAME:           main
**
** DESCRIPTION:    Main command dispatcher.
**
** RETURNS:        POSIX status code.
**
**------------------------------------------------------------------------*/
int main(int argc, const char *argv[])
{

	if (argc < 2)
	{
		/* TODO */
    	fprintf(stderr,"Incorrect usage\n");
    	return (EXIT_FAILURE);
  	}

  	/* Strip the program name before passing to command handler */
  	++argv;

  	return handleCommand(argc, argv);
}
