/*---------------------------------------------------------------------------
**
** MODULE NAME:     led_daemon.c
**
** COPYRIGHT:       Copyright (c) 2014, Clare Controls, Inc.
**                  All Rights Reserved.
**
**                  NOTICE: THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF
**                  CLARE CONTROLS, INC., AND SHALL NOT BE REPRODUCED, COPIED
**                  IN WHOLE OR IN PART ADAPTED, MODIFIED, OR
**                  DISSEMINATED WITHOUT THE EXPRESS WRITTEN CONSENT
**                  OF CLARE CONTROLS, INC. ANY REDISTRIBUTION OR
**                  PUBLICATION TO UNAUTHORIZED PERSONS IS STRICTLY
**                  PROHIBITED BY U.S. AND INTERNATIONAL COPYRIGHT LAW.
**
** DESCRIPTION:     Red/Blue LED event handler daemon. Modified code
**					originally written for cliq.host/express led control.
**
** AUTHOR:          Doug Budinsky
**
** CREATION DATE:   Dec 15, 2015
**
**---------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/inotify.h>
#include <sys/stat.h>
#include <syslog.h>
#include <pthread.h>
#include <time.h>
#include <wiringPi.h>

#include "led-daemon.h"

#define MODULE_NAME   "led_daemon: "

static pthread_mutex_t  gListMutex = PTHREAD_MUTEX_INITIALIZER;
static LED_EVENT_CONFIG event_list[LED_TOTAL_EVENTS];

/*---------------------------------------------------------------------------
**
** NAME:           writeRedLed
**
** DESCRIPTION:    Write boolean value to the red LED.
**
** RETURNS:        Bool indicating successful write or not.
**
**---------------------------------------------------------------------------*/
static BOOL writeRedLed(BOOL state)
{
	int         rc = TRUE;
	const char *p_state = NULL;

	switch (state)
	{
    	case TRUE:
			digitalWrite(RED_LED, 1);
     		break;
		case FALSE:
			digitalWrite(RED_LED, 0);
			break;
    	default:
      		rc = FALSE;
      		break;
  	}
	return (rc);
}

/*---------------------------------------------------------------------------
**
** NAME:           writeBlueLed
**
** DESCRIPTION:    Write boolean value to the blue LED.
**
** RETURNS:        Bool indicating successful write or not.
**
**---------------------------------------------------------------------------*/
static BOOL writeBlueLed(BOOL state)
{
	int         rc = TRUE;
	const char *p_state = NULL;

	switch (state)
	{
    	case TRUE:
			digitalWrite(BLUE_LED, 1);
     		break;
		case FALSE:
			digitalWrite(BLUE_LED, 0);
			break;
    	default:
      		rc = FALSE;
      		break;
  	}
	return (rc);
}

/*---------------------------------------------------------------------------
**
** NAME:           readLedEventList
**
** DESCRIPTION:    Read in the LED event list file.
**
** RETURNS:        Boolean.
**
**---------------------------------------------------------------------------*/
static BOOL readLedEventList(void)
{
  int fd = open(LED_EVENT_LIST, O_RDONLY);
  if (fd < 0)
  {
    syslog(LOG_EMERG, "Can't open for reading LED event list %s: %s",
           LED_EVENT_LIST, strerror(errno));
    return (FALSE);
  }
  else
  {
    struct flock flock;

    flock.l_type   = F_RDLCK;
    flock.l_whence = SEEK_SET;
    flock.l_start  = 0;
    flock.l_len    = 0;

    if (fcntl(fd, F_SETLKW, &flock) != 0)
    {
      syslog(LOG_EMERG, "Can't open for reading LED event list %s: %s",
             LED_EVENT_LIST, strerror(errno));
      close(fd);
      return (FALSE);
    }
    else
    {
      LED_EVENT_CONFIG buf[LED_TOTAL_EVENTS];

      if (read(fd, buf, sizeof(buf)) != sizeof(buf))
      {
        syslog(LOG_EMERG, "Can't read LED event list %s: %s",
               LED_EVENT_LIST, strerror(errno));
        close(fd);
        return (FALSE);
      }

      pthread_mutex_lock(&gListMutex);
      memcpy(event_list, buf, sizeof(event_list));
      pthread_mutex_unlock(&gListMutex);
    }
    
    close(fd);
    return (TRUE);
  }
}

/*---------------------------------------------------------------------------
**
** NAME:           getTime(struct timespec *t_time)
**
** DESCRIPTION:   Gets the time filling the timespec structure
**
** RETURNS:       No return code
**
**---------------------------------------------------------------------------*/
static void getTime(struct timespec *time)
{
	clock_gettime(CLOCK_MONOTONIC, time);
}

/*---------------------------------------------------------------------------
**
** NAME:           diffTime
**
** DESCRIPTION:    return time difference in milliseconds of 2 timespec 
**					structures.
**
** RETURNS:        double difference of time in milliseconds
**
**---------------------------------------------------------------------------*/
static double diffTime(struct timespec *t_past, struct timespec *t_present)
{
	return ((double)t_past->tv_sec + 1.0e-9*t_past->tv_nsec)-((double)t_present->tv_sec + 1.0e-9*t_present->tv_nsec);
}

/*---------------------------------------------------------------------------
**
** NAME:           processLedEvents
**
** DESCRIPTION:    Choose and execute the appropriate LED action.
**
** RETURNS:        TODO:POSIX status code.
**
**---------------------------------------------------------------------------*/
static BOOL processLedEvents(void)
{
  	static BOOL      toggle = TRUE;
  	static int       past_state = NORMAL;
        	int       present_state;
	static struct timespec past_Time={0,0}, present_Time={0,0};
  	
	LED_EVENT_CONFIG event_buf[LED_TOTAL_EVENTS]; 

  	/* Refresh the event list */
  	if (!readLedEventList())
    	return (FALSE);

  	pthread_mutex_lock(&gListMutex);
  	memcpy(event_buf, event_list, sizeof(event_buf));
  	pthread_mutex_unlock(&gListMutex);

  	if (event_buf[SHUTDOWN].state == TRUE)
    	present_state = SHUTDOWN;
  	else if (event_buf[BOOT].state == TRUE)
    	present_state = BOOT;
  	else if (event_buf[APMODE].state == TRUE)
    	present_state = APMODE;
  	else if (event_buf[ETHERNET].state == TRUE)
    	present_state = ETHERNET;
	else if (event_buf[UPDATE].state == TRUE)
		present_state=UPDATE;
  	else if (event_buf[CLAREHOME].state == TRUE)
    	present_state = CLAREHOME;
    else if (event_buf[NODNS].state == TRUE)
		present_state = NODNS;
  	else
    	present_state = NORMAL;
  
  	/* This re-inits timer between state changes */
  	if (present_state != past_state)
  	{
    	toggle = TRUE;
		getTime(&past_Time);
  	}

	getTime(&present_Time);

  switch (present_state)
  {
    case ETHERNET:
      	/* Blink Red */   
      	if (diffTime(&present_Time, &past_Time) >= 2)
      	{
        	toggle = !toggle;
			getTime(&past_Time);
      	}

      	if (toggle)
      	{
        	writeRedLed(TRUE);
        	writeBlueLed(FALSE); 
      	}
      	else
      	{
        	writeRedLed(FALSE);
        	writeBlueLed(FALSE); 
      	}
      	break;
    case CLAREHOME:
      	/* Solid Red */
      	writeBlueLed(FALSE); 
      	writeRedLed(TRUE);
      	break;
    case BOOT:
      	if (diffTime(&present_Time, &past_Time) >= 1)
      	{
        	toggle = !toggle;
			getTime(&past_Time);
      	}

      	if (toggle)
      	{
        	writeBlueLed(FALSE); 
        	writeRedLed(FALSE);
      	}
      	else
      	{
        	writeBlueLed(TRUE); 
        	writeRedLed(FALSE);
      	}
      	break;
    case APMODE:
      	if (diffTime(&present_Time, &past_Time) >= .50)
      	{
        	toggle = !toggle;
			getTime(&past_Time);
      	}

      	if (toggle)
      	{
        	writeRedLed(TRUE);
        	writeBlueLed(FALSE); 
      	}
      	else
      	{
        	writeRedLed(FALSE);
        	writeBlueLed(FALSE); 
      	}
      	break;
    case SHUTDOWN:
		if (diffTime(&present_Time, &past_Time) >= .25)
		{
			toggle = !toggle;
			getTime(&past_Time);
		}

      	if (toggle)
      	{
        	writeRedLed(TRUE);
        	writeBlueLed(FALSE); 
      	}
      	else
      	{
        	writeRedLed(FALSE);
        	writeBlueLed(TRUE); 
      	}
      	break;
    case NODNS:
		if (diffTime(&present_Time, &past_Time) >= .50)
		{
			toggle = !toggle;
			getTime(&past_Time);
		}

      	if (toggle)
      	{
       		writeRedLed(FALSE);
        	writeBlueLed(FALSE); 
      	}
      	else
      	{
       		writeRedLed(FALSE);
        	writeBlueLed(TRUE); 
      	}
		break;
    case UPDATE:
		if (diffTime(&present_Time, &past_Time) >= .50)
		{
			toggle = !toggle;
			getTime(&past_Time);
		}

      	if (toggle)
      	{
       		writeRedLed(TRUE);
        	writeBlueLed(TRUE); 
      	}
      	else
      	{
       		writeRedLed(FALSE);
        	writeBlueLed(FALSE); 
      	}
		break;
    case NORMAL:
      	/* Clare Blue LED */
      	writeRedLed(FALSE);
      	writeBlueLed(TRUE); 
      	break;
    default:
      	break;
  }

  past_state = present_state;
  return (TRUE);
}

/*---------------------------------------------------------------------------
**
** NAME:           initLedList
**
** DESCRIPTION:    Initialize the LED event list.
**
** RETURNS:        POSIX status code.
**
**------------------------------------------------------------------------*/
static BOOL initLedList(void)
{
  //LED_EVENT_CONFIG event[5] = {FALSE};

  int fd = open(LED_EVENT_LIST, O_RDWR | O_CREAT, 00666);

  if (fd < 0)
  {
    syslog(LOG_INFO, "Can't open for initializing LED event list %s: %s", 
           LED_EVENT_LIST, strerror(errno));  
    return (FALSE);
  }
  else
  {

	// change permissions of the file so everyone has rw privs.
	chmod(LED_EVENT_LIST, S_IWOTH|S_IROTH|S_IRGRP|S_IWGRP|S_IWUSR|S_IRUSR);

    struct flock flock;
   
    flock.l_type = F_WRLCK;
    flock.l_whence = SEEK_SET;
    flock.l_start = 0;
    flock.l_len = 0;

    if (fcntl(fd, F_SETLKW, &flock) != 0)
    {
      syslog(LOG_INFO, "Can't lock LED event list %s: %s", 
             LED_EVENT_LIST, strerror(errno));  
      close(fd);
      return (FALSE);
    }
    else
    {
      int zeros[36] = {0};
		zeros[8] = 1;
      if (write(fd, zeros, sizeof(zeros)) != sizeof(zeros))
      {
        syslog(LOG_INFO, "Can't initialize LED event list %s: %s", 
               LED_EVENT_LIST, strerror(errno));  
        close(fd);
        return (FALSE);
      }
    } 
    
    close(fd);
  }

  return (TRUE);
}

/*---------------------------------------------------------------------------
**
** NAME:           Daemonize
**
** DESCRIPTION:    Posix daemon startup routine.
**
** RETURNS:        void
**
**---------------------------------------------------------------------------*/
static void Daemonize(void)
{
  static const char dev_null[] = "/dev/null";
  int  maxfd, i, pid_file;
  char pid[32];

  /* First do a detaching fork. */
  if (fork() != 0)
    _exit(0);

  /* Become a new session leader. */
  setsid();

  /*
   * Now fork again so the parent (now the session group leader) can exit.
   * This means that we, as a non-session group leader, can (never again)
   * regain a controlling terminal. 
   */
  if (fork() != 0)
    _exit(0);

  /* Now close off any inherited file handles */
  maxfd = sysconf(_SC_OPEN_MAX);
  if (maxfd < 0)
    maxfd = 20; /* Best guess. */
  for (i = 0; i < maxfd; i++)
    close(i);

  /* Now setup our standard descriptors to /dev/null. */
  open(dev_null, O_RDONLY);
  open(dev_null, O_WRONLY);
  open(dev_null, O_WRONLY);

  /* Give us a sensible umask. */
  umask(023);
 
  /*
   * Switch to the root directory to avoid keeping any unnecessary
   * vnodes busy.
   */
   chdir("/");

  /* We want to use syslog to get details. */
  openlog("led-daemon",
          LOG_PID | LOG_NDELAY | LOG_NOWAIT, 
          LOG_LOCAL0);

  /* Write out PID to a file. */
  sprintf(pid, "%ld\n", (long )getpid());
  pid_file = open("/var/run/led-daemon.pid", O_CREAT | O_WRONLY, 00666);
  if (pid_file >= 0)
  {
    write(pid_file, pid, strlen(pid));
    close(pid_file);
  }
}

/*---------------------------------------------------------------------------
**
** NAME:           main
**
** DESCRIPTION:    Main entry point for LED daemon process
**
** RETURNS:        TODO:POSIX status code.
**
**---------------------------------------------------------------------------*/
int main(int argc, const char *argv[])
{
	Daemonize();
	syslog(LOG_INFO, "LED event daemon started");

	if (wiringPiSetup () == -1)
	{
		syslog (LOG_NOTICE, "Failed to load wiringPi library");
		return 1;
	}

	if (!initLedList())
	{
		syslog(LOG_INFO, MODULE_NAME "Can't init led list");
		return (EXIT_FAILURE);
	}

	// Set the gpio mode
	pinMode(RED_LED, OUTPUT);
	pinMode(BLUE_LED, OUTPUT);

	while (1)
	{
		processLedEvents();
		usleep(150000);
	}
}
