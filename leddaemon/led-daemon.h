#ifndef _LED_H_
#define _LED_H_

#define LED_EVENT_LIST      "/var/run/led-events.list"
#define LED_TOTAL_EVENTS    8
#define BLUE_LED            2
#define RED_LED             0

/* Address offsets for LED event list */
#define ETHERNET_ADDR		0
#define CLAREHOME_ADDR		16
#define BOOT_ADDR			32
#define APMODE_ADDR			48
#define SHUTDOWN_ADDR		64
#define NODNS_ADDR			80
#define UPDATE_ADDR			96

/*
#define SHUTDOWN_ADDR		0
#define ETHERNET_ADDR		16
#define CLAREHOME_ADDR		32
#define BOOT_ADDR			48
#define APMODE_ADDR			64
#define NODNS_ADDR			80
*/

typedef enum
{
  ETHERNET,
  CLAREHOME,
  BOOT,
  APMODE,
  SHUTDOWN,
  NODNS,
  UPDATE,
  NORMAL
} LED_EVENTS;

typedef enum { FALSE, TRUE } BOOL;

/* 
 * We use 12 dummy bytes to fill out the rest of the address line
 * in the the list file. Each address line contains 16 bytes. The
 * BOOL is represented as an integer (4 bytes on our system).
 */
typedef struct
{
  BOOL  state;
  int   dummy[3];
} LED_EVENT_CONFIG;

#endif /* _LED_H_ */
