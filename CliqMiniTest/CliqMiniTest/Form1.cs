﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Mono.Zeroconf;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using bpac;

namespace CliqMiniTest
{
    public partial class Form1 : Form
    {
        BackgroundWorker backgroundWorker1;

        delegate void SetDeviceCallback(string text);

        public Form1()
        {
            InitializeComponent();

            // Initialize the ZeroConf browser
            ServiceBrowser browser = new ServiceBrowser();
            browser.ServiceAdded += OnServiceAdded;
            browser.ServiceRemoved += OnServiceRemoved;

            //
            // Trigger the request
            //
            browser.Browse("_mfg._tcp", "local");

        }

        // Callback for adding a zeroconfig device
        private void OnServiceAdded(object o, ServiceBrowseEventArgs args)
        {
            this.AddDevice(args.Service.Name); 
        }

        // Callback for remvoing a zeroconfig device
        private void OnServiceRemoved( object o, ServiceBrowseEventArgs args)
        {
            this.RemoveDevice(args.Service.Name);
        }

        // Thread-safe method for adding a device to the listview
        private void AddDevice(string text)
        {
            if (this.iplistView.InvokeRequired)
            {
                SetDeviceCallback d = new SetDeviceCallback(AddDevice);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.iplistView.Items.Add(text);
            }

        }

        // Thread-safe method for removing a device from the listview
        private void RemoveDevice(string text)
        {
            if (this.iplistView.InvokeRequired)
            {
                SetDeviceCallback d = new SetDeviceCallback(RemoveDevice);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                ListViewItem index = iplistView.FindItemWithText(text);
                this.iplistView.Items.Remove(index);
            }

        }

        // Callback thread-safe background worker thread used
        // for both adding and removing devices.
        private void setAddDeviceBackgroundWorker(object sender, EventArgs e)
        {
            this.backgroundWorker1.RunWorkerAsync();
        }

        private void iplistView_SelectedIndexChanged(object sender, EventArgs e)
        {

            
        }

        // Process the mac address we got from the unit.  This will give us
        // a Clare controls specific mac address when called from the unit.  It
        // should never be a raspberry Pi mac.
        private void processMac()
        {
            // Grab the mac address from the unit
            try
            {
                string locationRequest = "http://" + txtIP.Text + ":3000/users/curmac";

                WebClient c = new WebClient();
                var data = c.DownloadString(locationRequest);

                JObject o = JObject.Parse(data);

                if (o != null)
                {
                    // Now we need to test to ensure  the mac address is either a clares or not.

                    // Strip out the colon's
                    string macaddress = o["macaddr"].ToString().Replace(":", "");
                    long nMacAddr = Convert.ToInt64(macaddress, 16);
                    if (97201545674752 <= nMacAddr && nMacAddr >= 97201546002431)
                    {
                        // We are outside out range, so we need to get a new mac address
                        string newMac = getNewMac();

                        StringBuilder sb = new StringBuilder();

                        int index = 0;

                        foreach (char cha in newMac)
                        {
                            sb.Append(cha);
                            if ((++index % 2) == 0) sb.Append(":");
                        }

                        newMac = sb.Remove(sb.Length - 1, 1).ToString();
                        txtMacAddr.Text = newMac;
                    }
                    else
                    {
                        txtMacAddr.Text = o["macaddr"].ToString();
                    }
                }
            }
            catch (WebException ece)
            {
                MessageBox.Show("Error communicating with unit, No response from unit, error: " + ece.Status.ToString());
            }
        }

        // Gets the MAC address of the unit under test.
        private string getNewMac()
        {
            string macaddr = "";
            try
            {
                string locationRequest = "http://" + txtIP.Text + ":3000/users/macaddr";
                WebClient c = new WebClient();
                var data = c.DownloadString(locationRequest);
                JObject o = JObject.Parse(data);
                if ( o != null)
                {
                    string number = o["macaddr"].ToString();
                    macaddr = number;
                }
            }
            catch ( WebException ece )
            {
                MessageBox.Show("Failed to get a new mac address - ERROR: " + ece.Status.ToString());
                macaddr = "";
            }
            return macaddr;
        }

        // Performs the test for UUT.
        private void btnTest_Click(object sender, EventArgs e)
        {
            setResultLabel("Testing", Color.Black);

            btnTest.Enabled = false;

            // Process the Mac Address
            processMac();

            

            try
            {
                string locationRequest = "http://" + txtIP.Text + ":3000/users/minitest";

                WebClient c = new WebClient();
                var data = c.DownloadString(locationRequest);

                JObject o = JObject.Parse(data);

                if (o != null)
                {
                    txtHostname.Text = o["Hostname"].ToString();
                    txtType.Text = o["Type"].ToString();
                    txtPlatform.Text = o["Platform"].ToString();
                    txtArch.Text = o["Architecture"].ToString();
                    txtRelease.Text = o["Release"].ToString();
                    txtOSUptime.Text = o["OS-Uptime"].ToString();
                    txtOSLoad.Text = o["OS-load-average"].ToString();
                    txtTotalRam.Text = o["Total-Ram"].ToString();
                    txtFreeRam.Text = o["Free-Ram"].ToString();
                    txtTempDir.Text = o["Temp-Directory"].ToString();
                    txtZwave.Text = o["Z-Wave"].ToString();
                    txtWiFi.Text = o["WiFi"].ToString();

                    if (txtZwave.Text.Contains("Fail") || txtWiFi.Text.Contains("Fail"))
                    {
                        setResultLabel("FAIL", Color.Red);
                        string locationRequest1 = "http://" + txtIP.Text + ":3000/users/detach";

                        WebClient c1 = new WebClient();
                        var data1 = c1.DownloadString(locationRequest1);
                    }
                    else
                    {
                        setResultLabel("PASS", Color.Green);

                        btnSave.Visible = true;
                        btnSave.Enabled = true;
                    }
                }
            }
            catch ( WebException ece)
            {   
                MessageBox.Show("Error communicating with unit, No response from unit, error: " + ece.Status.ToString());
            }

            btnPrint.Enabled = true;
        }

        // Clears the text boxes of data.
        private void clearTest()
        {
            //txtIP.Clear();
            txtMacAddr.Clear();
            txtHostname.Clear();
            txtType.Clear();
            txtPlatform.Clear();
            txtArch.Clear();
            txtRelease.Clear();
            txtOSUptime.Clear();
            txtOSLoad.Clear();
            txtTotalRam.Clear();
            txtFreeRam.Clear();
            txtTempDir.Clear();
            txtZwave.Clear();
            txtWiFi.Clear();
            btnSave.Visible = false;
            lblRESULT.Text = "";
        }

        // Performs the saving and provisioning of the unit under test.
        private void btnSave_Click(object sender, EventArgs e)
        {
            // Disable the save button so we cannot hit it multiple times.
            setResultLabel("Saving", Color.Black);

            // Create the database json object with data.
            JObject jsonObj = new JObject();

            
            jsonObj.Add("MacAddress", txtMacAddr.Text);
            jsonObj.Add("Hostname", txtHostname.Text);
            jsonObj.Add("Type", txtType.Text);
            jsonObj.Add("Platform", txtPlatform.Text);
            jsonObj.Add("Architecture", txtArch.Text);
            jsonObj.Add("Release", txtRelease.Text);
            jsonObj.Add("OS-Uptime", txtOSUptime.Text);
            jsonObj.Add("OS-load-average", txtOSLoad.Text.Trim('\r').Trim('\n'));
            jsonObj.Add("Total-Ram", txtTotalRam.Text);
            jsonObj.Add("Free-Ram", txtFreeRam.Text);
            jsonObj.Add("Temp-Directory", txtTempDir.Text);
            jsonObj.Add("Z-Wave", txtZwave.Text);
            jsonObj.Add("WiFi", txtWiFi.Text);

            btnSave.Enabled = false;

            try
            {
                WebClient client = new WebClient();

                var dataString = JsonConvert.SerializeObject(jsonObj);
                client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                client.UploadString("http://" + txtIP.Text + ":3000/users/savemini", "POST", dataString);
            }
            catch ( WebException ece )
            {
                lblRESULT.ForeColor = Color.Red;
                lblRESULT.Text = "Failed Saving";
                MessageBox.Show("Failed to get response - Error: " + ece.Status.ToString());
            }

            // Add webcall to increment the mac in the database
            try
            {
                string locationRequest = "http://" + txtIP.Text + ":3000/users/savemacaddr";

                WebClient c = new WebClient();
                var data = c.DownloadString(locationRequest);
            }
            catch ( WebException ece)
            {
                MessageBox.Show("Failed to iterate mac address to database - ERROR: " + ece.Status.ToString());
            }

            btnSave.Visible = false;
            btnSave.Enabled = true;
            setResultLabel("Done", Color.Black);
            //btnPrint.Visible = true;
            //btnPrint.Enabled = true;


        }

        private bool checkPrinter()
        {
            bool bResult = false;

            bpac.DocumentClass doc = new DocumentClass();
            object[] printers = (object[])doc.Printer.GetInstalledPrinters();

            // Aquire the printer name
            string printerName = printers[0].ToString();

            // Check the printer is online
            if (doc.Printer.IsPrinterOnline(printerName))
            {
                bResult = true;
            }
            else
            {
                MessageBox.Show("No Printer found... check to ensure printer is connected and turned on");
            }
            return bResult;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (checkPrinter())
            {
                printLabel();
            }
        }

        // Prints a label out to the zebra printer.
        private void printLabel()
        {
            string strFilePath = "C:\\Labels\\cliqminilabel.lbx";

            bpac.DocumentClass doc = new bpac.DocumentClass();

            DateTime time = DateTime.Now;
            string format = "yyyyMMd";
            //console.Text = time.ToString(format);

            // Open template
            doc.Open(strFilePath);

            doc.GetObject("macaddr").Text = txtMacAddr.Text;
            doc.GetObject("txtdate").Text = time.ToString(format);
            doc.StartPrint("", PrintOptionConstants.bpoDefault);
            doc.PrintOut(1, PrintOptionConstants.bpoDefault);
            doc.EndPrint();

            doc.Close();
        }

        private void printerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrintCfgDlg dlg = new PrintCfgDlg();
            dlg.Show();
        }

        private void iplistView_ItemActivate(object sender, EventArgs e)
        {
            //clearTest();

            //txtIP.Text = iplistView.Items[0].Text;

            if (iplistView.Items.Count != 0)
            {
                btnTest.Enabled = true;
                btnSave.Enabled = false;
                btnSave.Visible = false;
                ListView.SelectedListViewItemCollection items = this.iplistView.SelectedItems;
                txtIP.Text = items[0].Text;
                clearTest();
            }
        }

        private void setResultLabel(string message, Color color)
        {
            lblRESULT.ForeColor = color;
            lblRESULT.Text = message;
            lblRESULT.Refresh();
        }

        private void btnPrintInserts_Click(object sender, EventArgs e)
        {
            if (checkPrinter())
            {
                PrintInserts();
            }
        }

        private void PrintInserts()
        {
            string strFilePath = "C:\\Labels\\cliqminiinsert.lbx";

            bpac.DocumentClass doc = new bpac.DocumentClass();

            // Open template
            doc.Open(strFilePath);

            doc.GetObject("macaddr2").Text = txtMacAddr.Text;
            
            doc.StartPrint("", PrintOptionConstants.bpoDefault);
            doc.PrintOut(5, PrintOptionConstants.bpoDefault);
            doc.EndPrint();

            doc.Close();
        }
    }
}
