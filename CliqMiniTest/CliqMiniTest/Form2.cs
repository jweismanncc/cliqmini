﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CliqMiniTest
{
    public partial class PrintCfgDlg : Form
    {
        public PrintCfgDlg()
        {
            InitializeComponent();

            // Loads the values from the current settings
            txtCnfIpAddr.Text = Properties.Settings.Default.PrinterIP.ToString();
            txtCnfPort.Text = Properties.Settings.Default.PrintPort.ToString();
        }

        private void btnCancelConfig_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSaveConfig_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default["PrinterIP"] = txtCnfIpAddr.Text;
            Properties.Settings.Default["PrintPort"] = txtCnfPort.Text;
            Properties.Settings.Default.Save();

            this.Close();
        }
    }
}
